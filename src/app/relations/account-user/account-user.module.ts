import { Module } from '@nestjs/common';
import { KnexModule } from '@modules/knex';
import { AccountUserRepository } from './repositories';
import { AccountUserService } from './services';

@Module({
  imports: [KnexModule.forRoot()],
  providers: [AccountUserRepository, AccountUserService],
  exports: [AccountUserService],
})
export class AccountUserModule {}
