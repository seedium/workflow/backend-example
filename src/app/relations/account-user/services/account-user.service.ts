import { Injectable } from '@nestjs/common';
import {
  ID,
  IList,
  IListOptions,
  IDataOptions,
  RepositoryDatabaseOptions,
  OmitDefaultAccountResourceFields,
  SelectRepositoryDatabaseOptions,
  RepositoryListOptionsCreated,
} from 'nestjs-postgres';
import { TransactionOptions } from '@modules/transaction';
import { AccountRecord } from '@app/account/interfaces';
import { User } from '@app/user/interfaces';
import { AccountUser, AccountUserRecord } from '../interfaces';
import { AccountUserRepository } from '../repositories';

@Injectable()
export class AccountUserService {
  constructor(private readonly accountUserRepository: AccountUserRepository) {}
  public async listAccountsByUserId(
    idUser: ID,
    options?: RepositoryListOptionsCreated,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<IList<AccountRecord>> {
    return this.accountUserRepository.listAccountsByUserId(
      idUser,
      options,
      databaseOptions,
    );
  }
  public async retrieveByAccountAndUser(
    idAccount: ID,
    idUser: ID,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<AccountUser | null> {
    return this.accountUserRepository.retrieveByAccountAndUser(
      idAccount,
      idUser,
      databaseOptions,
    );
  }
  public async retrieveByEmailInAccount(
    idAccount: ID,
    email: string,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<User | null> {
    return this.accountUserRepository.retrieveByEmailInAccount(
      idAccount,
      email,
      databaseOptions,
    );
  }
  public async list<T extends AccountUser>(
    idAccount: ID,
    options?: IListOptions,
    databaseOptions?: SelectRepositoryDatabaseOptions<
      AccountUserRecord,
      AccountUser
    >,
  ): Promise<IList<T>> {
    return this.accountUserRepository.list<T>(
      idAccount,
      options,
      databaseOptions,
    );
  }
  public async create(
    idAccount: ID,
    accountUserPayload: OmitDefaultAccountResourceFields<AccountUserRecord>,
    transactionOptions?: TransactionOptions,
  ): Promise<AccountUserRecord> {
    return this.accountUserRepository.create(idAccount, accountUserPayload, {
      transaction: transactionOptions?.knexTrx,
    });
  }
  public async retrieve<T extends AccountUser>(
    idAccount: ID,
    idAccountUser: ID,
    options?: IDataOptions,
    databaseOptions?: SelectRepositoryDatabaseOptions<
      AccountUserRecord,
      AccountUser
    >,
  ): Promise<T | null> {
    return this.accountUserRepository.retrieve(
      idAccount,
      idAccountUser,
      options,
      databaseOptions,
    );
  }
  public async update(
    idAccount: ID,
    idAccountUser: ID,
    accountUserPayload: Partial<
      OmitDefaultAccountResourceFields<AccountUserRecord>
    >,
    transactionOptions?: TransactionOptions,
  ): Promise<AccountUserRecord | null> {
    return this.accountUserRepository.update(
      idAccount,
      idAccountUser,
      accountUserPayload,
      {
        transaction: transactionOptions?.knexTrx,
      },
    );
  }
  public async delete(
    idAccount: ID,
    idAccountUser: ID,
    transactionOptions?: TransactionOptions,
  ): Promise<AccountUserRecord | null> {
    return this.accountUserRepository.delete(idAccount, idAccountUser, {
      transaction: transactionOptions?.knexTrx,
    });
  }
}
