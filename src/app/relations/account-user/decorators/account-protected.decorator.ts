import { applyDecorators, UseGuards } from '@nestjs/common';
import { HeadersSchema } from '@lib/decorators/swagger';
import { headersSchema } from '@lib/schemas';
import { AccountGuard } from '../guards';

export const AccountProtected = (): (<TFunction extends Function, Y>(
  target: object | TFunction,
  propertyKey?: string | symbol,
  descriptor?: TypedPropertyDescriptor<Y>,
) => void) => {
  return applyDecorators(HeadersSchema(headersSchema), UseGuards(AccountGuard));
};
