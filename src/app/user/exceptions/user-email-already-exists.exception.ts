import { UserBaseException } from './user-base.exception';

export class UserEmailAlreadyExistsException extends UserBaseException {
  code = 'user_email_already_exists';
  status = 400;
  constructor(email?: string) {
    super('User' + (email ? ` with email "${email}"` : '') + ' already exists');
  }
}
