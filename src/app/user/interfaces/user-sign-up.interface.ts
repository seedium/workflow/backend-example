export interface UserSignUpPayload {
  email: string;
  first_name?: string;
  last_name?: string;
  city?: string;
  country?: string;
  avatar_link?: string;
  timezone?: string;
}

export interface UserSignupCompletePayload extends UserSignUpPayload {
  sub: string;
}
