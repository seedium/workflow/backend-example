import {
  PostgresError,
  SoftRepository,
  EntityRepository,
  RepositoryDatabaseOptions,
  OmitDefaultResourceFields,
} from 'nestjs-postgres';
import { UserEmailAlreadyExistsError } from '../errors';
import { UserRecord, User, CompletedUser } from '../interfaces';

@EntityRepository({ name: 'users' })
export class UserRepository extends SoftRepository<UserRecord, User> {
  public async retrieveByEmail(
    email: string,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<User | null> {
    return this._repository.hooks.retrieve(
      this._repository
        .queryBuilder(databaseOptions?.transaction)
        .select('*')
        .where('email', email)
        .whereNull('deleted_at')
        .first(),
    );
  }

  public async retrieveBySubjectId(
    sub: string,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<CompletedUser | null> {
    return this._repository.hooks.retrieve(
      this._repository
        .queryBuilder(databaseOptions?.transaction)
        .select('*')
        .where('sub', sub)
        .whereNull('deleted_at')
        .first(),
    );
  }

  public async create(
    createUserPayload: OmitDefaultResourceFields<UserRecord>,
    databaseOptions?: RepositoryDatabaseOptions,
  ): Promise<UserRecord> {
    try {
      return await super.create(createUserPayload, databaseOptions);
    } catch (err) {
      if (
        err instanceof PostgresError &&
        err.constraint === 'users_email_unique'
      ) {
        throw new UserEmailAlreadyExistsError(createUserPayload.email);
      }
      throw err;
    }
  }
}
