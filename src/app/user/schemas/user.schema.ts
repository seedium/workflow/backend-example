import S from 'fluent-json-schema';
import { resourceObjectSchema } from '@lib/schemas';

export const userDtoSchema = S.object()
  .additionalProperties(false)
  .prop('email', S.string())
  .prop('first_name', S.string())
  .prop('last_name', S.string())
  .prop('avatar_link', S.string())
  .prop('timezone', S.string())
  .prop('city', S.string())
  .prop('country', S.string())
  .extend(resourceObjectSchema);
