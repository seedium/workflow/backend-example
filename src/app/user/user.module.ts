import { forwardRef, Module } from '@nestjs/common';
import { KnexModule } from '@modules/knex';
import { TransactionModule } from '@modules/transaction/transaction.module';
import { AuthModule } from '@app/auth/auth.module';
import { AccountUserModule } from '@app/relations/account-user/account-user.module';
import { UserRepository } from './repositories';
import { UserService } from './services';
import { ProfileController } from './controllers';

@Module({
  imports: [
    KnexModule.forRoot(),
    forwardRef(() => AuthModule),
    AccountUserModule,
    TransactionModule,
  ],
  controllers: [ProfileController],
  providers: [UserRepository, UserService],
  exports: [UserService],
})
export class UserModule {}
