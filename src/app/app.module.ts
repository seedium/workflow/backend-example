import { Module } from '@nestjs/common';
import { ValidationModule } from 'nestjs-validation';
import { LoggerModule } from '@modules/logger/logger.module';
import { ConfigModule } from '@modules/config/config.module';
import { CqrsModule } from '@modules/cqrs/cqrs.module';
import { CoreModule } from '@modules/core/core.module';
import { HealthCheckModule } from '@modules/health-check/health-check.module';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { AccountModule } from './account/account.module';
import { AccountUserModule } from './relations/account-user/account-user.module';
import { NotificationModule } from './notification/notification.module';

@Module({
  imports: [
    ConfigModule,
    LoggerModule,
    ValidationModule.forRoot(),
    CqrsModule.forRoot(),
    CoreModule,
    HealthCheckModule,
    AccountModule,
    AuthModule,
    NotificationModule,
    AccountUserModule,
    UserModule,
  ],
})
export class AppModule {}
