import { Observable } from 'rxjs';
import { ofType, Saga } from '@nestjs/cqrs';
import { map } from 'rxjs/operators';
import { ICommand, IEvent } from '@modules/cqrs';
import { InvitationCreatedEvent } from '@app/account/events/impl';
import { SendMailCommand } from '@app/notification/commands/impl';

export class InvitationSaga {
  @Saga()
  public invitationCreatedSendMail(
    $events: Observable<IEvent>,
  ): Observable<ICommand> {
    return $events.pipe(
      ofType(InvitationCreatedEvent),
      map(
        ({ invitation, metadata }) =>
          new SendMailCommand(
            invitation.email,
            {
              text: 'Hello world',
              subject: 'Invitation',
            },
            metadata,
          ),
      ),
    );
  }
}
