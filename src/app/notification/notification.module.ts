import { Module } from '@nestjs/common';
import { MailModule } from '@modules/mail/mail.module';
import { SendMailHandler } from './commands/handlers';
import { InvitationSaga } from './sagas';

@Module({
  imports: [MailModule],
  providers: [InvitationSaga, SendMailHandler],
})
export class NotificationModule {}
