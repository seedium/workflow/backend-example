import { BaseEvent, IEvent } from '@modules/cqrs';
import { MetadataContainer } from '@modules/core/metadata';
import { AccountRecord } from '../../interfaces';

export class AccountCreatedEvent extends BaseEvent implements IEvent {
  static fromJson(message: string): AccountCreatedEvent {
    const {
      id,
      data: { object },
      request,
    } = BaseEvent.parseEventMessage<AccountRecord>(message);
    return new AccountCreatedEvent(
      object,
      BaseEvent.eventRequestToMetadata(request),
      id,
    );
  }

  constructor(
    public readonly account: AccountRecord,
    metadata: MetadataContainer,
    id?: string,
  ) {
    super(metadata, id);
  }

  public toJson(): string {
    return super.toJson(this.account);
  }
}
