import { BaseEvent, IEvent } from '@modules/cqrs';
import { MetadataContainer } from '@modules/core/metadata';
import { AccountRecord } from '../../interfaces';

export class AccountDeletedEvent extends BaseEvent implements IEvent {
  static fromJson(message: string): AccountDeletedEvent {
    const {
      id,
      request,
      data: { object },
    } = BaseEvent.parseEventMessage<AccountRecord>(message);
    return new AccountDeletedEvent(
      object,
      BaseEvent.eventRequestToMetadata(request),
      id,
    );
  }
  constructor(
    public readonly account: AccountRecord,
    metadata: MetadataContainer,
    id?: string,
  ) {
    super(metadata, id);
  }
  public toJson(): string {
    return super.toJson(this.account);
  }
}
