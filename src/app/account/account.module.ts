import { Module } from '@nestjs/common';
import { KnexModule } from '@modules/knex';
import { AuthModule } from '@app/auth/auth.module';
import { UserModule } from '@app/user/user.module';
import { AccountUserModule } from '@app/relations/account-user/account-user.module';
import { TransactionModule } from '@modules/transaction/transaction.module';
import {
  AccountController,
  InvitationController,
  InvitationUserController,
} from './controllers';
import { InvitationRepository, AccountRepository } from './repositories';
import { InvitationService, AccountService } from './services';

@Module({
  imports: [
    KnexModule.forRoot(),
    AuthModule,
    UserModule,
    AccountUserModule,
    TransactionModule,
  ],
  controllers: [
    AccountController,
    InvitationController,
    InvitationUserController,
  ],
  providers: [
    AccountService,
    InvitationService,
    AccountRepository,
    InvitationRepository,
  ],
  exports: [AccountService],
})
export class AccountModule {}
