import { Post, Get, Param, Body, Delete, Query } from '@nestjs/common';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import { ID, IDataOptions, IList, IListOptionsCreated } from 'nestjs-postgres';
import { httpAssert, isError } from '@utils';
import { scopes } from '@config/scopes.config';
import { RestController, RestMethod } from '@modules/core/decorators';
import { ResourceMissingException } from '@lib/exceptions/api';
import { AuthProtected } from '@app/auth/decorators';
import { Metadata, MetadataContainer } from '@modules/core/metadata';
import { AccountProtected } from '@app/relations/account-user/decorators';
import { listResponseSchema } from '@lib/schemas';
import {
  UserAlreadyInAccountError,
  UserAlreadyInvitedError,
} from '@app/account/errors';
import {
  UserAlreadyInAccountException,
  UserAlreadyInvitedException,
} from '@app/account/exceptions';
import { InvitationService } from '../services';
import {
  Invitation,
  InvitationRecord,
  CreateInvitationBodyDto,
} from '../interfaces';
import { createInvitationBodyDtoSchema } from '../schemas/invitation-controller.schema';
import { invitationDtoSchema } from '../schemas/invitation.schema';
import { TargetAccount } from '../decorators';

@ApiTags('Invitations')
@RestController('invitations', AuthProtected(), AccountProtected())
export class InvitationController {
  constructor(private readonly invitationService: InvitationService) {}

  @Get()
  @ApiOperation({ summary: 'List of account invitations' })
  @RestMethod({
    scopes: [scopes.invitations.list],
    responses: {
      200: {
        schema: listResponseSchema(invitationDtoSchema),
      },
    },
  })
  public async list(
    @TargetAccount() idAccount: ID,
    @Query() options?: IListOptionsCreated,
  ): Promise<IList<Invitation>> {
    return this.invitationService.list(idAccount, options);
  }

  @Post()
  @ApiOperation({ summary: 'Create an account invitation' })
  @RestMethod({
    scopes: [scopes.invitations.create],
    body: createInvitationBodyDtoSchema,
    responses: { 201: { schema: invitationDtoSchema } },
  })
  public async create(
    @TargetAccount() idAccount: ID,
    @Body() createInvitationBodyDto: CreateInvitationBodyDto,
    @Metadata() metadata: MetadataContainer,
  ): Promise<InvitationRecord> {
    const invitationOrError = await this.invitationService.createFromDto(
      idAccount,
      createInvitationBodyDto,
      metadata,
    );
    httpAssert(
      !(invitationOrError instanceof UserAlreadyInvitedError),
      new UserAlreadyInvitedException('User already invited'),
    );
    httpAssert(
      !(invitationOrError instanceof UserAlreadyInAccountError),
      new UserAlreadyInAccountException('User already exists in account'),
    );
    httpAssert(!isError(invitationOrError), invitationOrError as Error);
    return invitationOrError;
  }

  @Get(':id_invitation')
  @ApiOperation({ summary: 'Retrieve an account invitation' })
  @RestMethod({
    scopes: [scopes.invitations.retrieve],
    responses: { 200: { schema: invitationDtoSchema } },
  })
  public async retrieve(
    @TargetAccount() idAccount: ID,
    @Param('id_invitation') idInvitation: ID,
    @Query() options?: IDataOptions,
  ): Promise<Invitation> {
    const invitation = await this.invitationService.retrieve(
      idAccount,
      idInvitation,
      options,
    );
    httpAssert(invitation, new ResourceMissingException());
    return invitation;
  }

  @Delete(':id_invitation')
  @ApiOperation({ summary: 'Deletes an account invitation' })
  @RestMethod({
    scopes: [scopes.invitations.delete],
    responses: { 200: { schema: invitationDtoSchema } },
  })
  public async delete(
    @TargetAccount() idAccount: ID,
    @Param('id_invitation') idInvitation: ID,
    @Metadata() metadata: MetadataContainer,
  ): Promise<InvitationRecord> {
    const invitation = await this.invitationService.delete(
      idAccount,
      idInvitation,
      metadata,
    );
    httpAssert(invitation, new ResourceMissingException());
    return invitation;
  }
}
