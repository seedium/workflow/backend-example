import {
  Post,
  Get,
  Patch,
  Param,
  Body,
  UseGuards,
  Delete,
} from '@nestjs/common';
import { ApiTags, ApiOperation } from '@nestjs/swagger';
import { ID } from 'nestjs-postgres';
import { httpAssert, isError } from '@utils';
import { scopes } from '@config/scopes.config';
import { RestController, RestMethod } from '@modules/core/decorators';
import { ResourceMissingException } from '@lib/exceptions/api';
import { AuthProtected } from '@app/auth/decorators';
import { Metadata, MetadataContainer } from '@modules/core/metadata';
import { AccountService } from '../services';
import {
  Account,
  CreateAccountBodyDto,
  UpdateAccountBodyDto,
} from '../interfaces';
import { AccountAlreadyExistsError } from '../errors';
import { AccountAlreadyExistsException } from '../exceptions';
import { VerifyAccountAccessFromParamGuard } from '../guards';
import {
  createAccountBodyDtoSchema,
  updateAccountBodyDtoSchema,
} from '../schemas/account-controller.schema';
import { accountDtoSchema } from '../schemas/account.schema';

@ApiTags('Accounts')
@RestController('accounts', AuthProtected())
export class AccountController {
  constructor(private readonly accountService: AccountService) {}

  @Post()
  @ApiOperation({ summary: 'Create an account' })
  @RestMethod({
    scopes: [scopes.accounts.create],
    body: createAccountBodyDtoSchema,
    responses: { 201: { schema: accountDtoSchema } },
  })
  public async create(
    @Body() createAccountBodyDto: CreateAccountBodyDto,
    @Metadata() metadata: MetadataContainer,
  ): Promise<Account> {
    const accountOrError = await this.accountService.createFromDto(
      createAccountBodyDto,
      metadata,
    );
    httpAssert(
      !(accountOrError instanceof AccountAlreadyExistsError),
      new AccountAlreadyExistsException(),
    );
    httpAssert(!isError(accountOrError), accountOrError as Error);
    return accountOrError;
  }

  @Get(':id_account')
  @ApiOperation({ summary: 'Retrieve an account' })
  @RestMethod(
    {
      scopes: [scopes.accounts.retrieve],
      responses: { 200: { schema: accountDtoSchema } },
    },
    UseGuards(VerifyAccountAccessFromParamGuard),
  )
  public async retrieve(@Param('id_account') idAccount: ID): Promise<Account> {
    const account = await this.accountService.retrieve(idAccount);
    httpAssert(account, new ResourceMissingException());
    return account;
  }

  @Patch(':id_account')
  @ApiOperation({ summary: 'Update an account' })
  @RestMethod(
    {
      scopes: [scopes.accounts.update],
      body: updateAccountBodyDtoSchema,
      responses: { 200: { schema: accountDtoSchema } },
    },
    UseGuards(VerifyAccountAccessFromParamGuard),
  )
  public async update(
    @Param('id_account') idAccount: ID,
    @Body() updateAccountBodyDto: UpdateAccountBodyDto,
    @Metadata() metadata: MetadataContainer,
  ): Promise<Account> {
    const account = await this.accountService.update(
      idAccount,
      updateAccountBodyDto,
      metadata,
    );
    httpAssert(account, new ResourceMissingException());
    return account;
  }

  @Delete(':id_account')
  @ApiOperation({ summary: 'Deletes an account' })
  @RestMethod(
    {
      scopes: [scopes.accounts.delete],
      responses: { 200: { schema: accountDtoSchema } },
    },
    UseGuards(VerifyAccountAccessFromParamGuard),
  )
  public async delete(
    @Param('id_account') idAccount: ID,
    @Metadata() metadata: MetadataContainer,
  ): Promise<Account> {
    const account = await this.accountService.delete(idAccount, metadata);
    httpAssert(account, new ResourceMissingException());
    return account;
  }
}
