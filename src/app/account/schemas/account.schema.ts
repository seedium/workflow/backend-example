import S from 'fluent-json-schema';
import { resourceObjectSchema } from '@lib/schemas';

export const accountDtoSchema = S.object()
  .additionalProperties(false)
  .prop('name', S.string())
  .extend(resourceObjectSchema);
