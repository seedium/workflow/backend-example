import S from 'fluent-json-schema';
import { emailSchema, enumSchema } from '@lib/schemas';
import { ACCOUNT_USER_ROLE } from '@app/relations/account-user/account-user.constants';

export const createInvitationBodyDtoSchema = S.object()
  .additionalProperties(false)
  .required(['email'])
  .prop('email', emailSchema)
  .prop('role', enumSchema(ACCOUNT_USER_ROLE));
