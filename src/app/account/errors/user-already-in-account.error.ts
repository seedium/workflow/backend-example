export class UserAlreadyInAccountError extends Error {
  constructor() {
    super('User already in the account');
  }
}
