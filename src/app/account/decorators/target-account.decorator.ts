import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { ApplicationFastifyRequest } from '@modules/core/interfaces';

export const TargetAccount = createParamDecorator(
  (_: unknown, context: ExecutionContext) => {
    const req = context.switchToHttp().getRequest<ApplicationFastifyRequest>();
    return req.idAccount ?? null;
  },
);
