import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ApplicationFastifyRequest } from '@modules/core/interfaces';
import {
  BadRequestException,
  ResourceMissingException,
} from '@lib/exceptions/api';
import { AccountUserService } from '@app/relations/account-user/services';
import { httpAssert } from '@utils';

@Injectable()
export class VerifyAccountAccessFromParamGuard implements CanActivate {
  constructor(private readonly accountUserService: AccountUserService) {}
  public async canActivate(context: ExecutionContext): Promise<boolean> {
    const req = context.switchToHttp().getRequest<ApplicationFastifyRequest>();
    const idAccount = (req.params as Record<string, string | undefined>)
      .id_account;
    if (!idAccount) {
      throw new BadRequestException('Could not find id account in params');
    }
    if (!req.user) {
      throw new Error('User not found');
    }
    const accountUser = await this.accountUserService.retrieveByAccountAndUser(
      idAccount,
      req.user.id,
    );
    httpAssert(accountUser, new ResourceMissingException());
    return true;
  }
}
