import {
  CanActivate,
  Injectable,
  ExecutionContext,
  Inject,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { scopesConfig, ScopesConfig } from '@config/scopes.config';
import { ApplicationFastifyRequest } from '@modules/core/interfaces';
import { CompletedUser } from '@app/user/interfaces';
import { AccountUserRole } from '@app/relations/account-user/interfaces';
import { ScopeService } from '@lib/services';
import { httpAssert } from '@utils';
import { ForbiddenException } from '../exceptions';
import {
  EXPECTED_SCOPES_TOKEN,
  EXPECTED_SCOPES_OPTIONS_TOKEN,
} from '../auth.constants';

export interface ScopesOptions {
  checkAllScopes?: boolean;
}

@Injectable()
export class ScopesGuard implements CanActivate {
  private readonly _scopeService = new ScopeService();
  constructor(
    private readonly reflector: Reflector,
    @Inject(scopesConfig.KEY)
    private readonly sc: ScopesConfig,
  ) {}
  public canActivate(context: ExecutionContext): boolean {
    const req = context
      .switchToHttp()
      .getRequest<ApplicationFastifyRequest<CompletedUser>>();
    const permissions = this.retrievePermissions(req);
    const expectedScopes = this.reflector.get<string[]>(
      EXPECTED_SCOPES_TOKEN,
      context.getHandler(),
    );
    const options = this.reflector.get<ScopesOptions>(
      EXPECTED_SCOPES_OPTIONS_TOKEN,
      context.getHandler(),
    );
    const isAllowed = this._scopeService.verify(
      expectedScopes,
      permissions,
      options,
    );
    httpAssert(isAllowed, new ForbiddenException(expectedScopes));
    req.permissions = permissions;
    return true;
  }
  private retrievePermissions(
    req: ApplicationFastifyRequest<CompletedUser>,
  ): string[] {
    if (req.user) {
      return this.retrieveUserPermissions();
    }
    return [];
  }
  private retrieveUserPermissions(): string[] {
    return [...this.sc.default, ...this.retrievePermissionsByRole('owner')];
  }
  private retrievePermissionsByRole(role: AccountUserRole): string[] {
    return this.sc.roles[role] ?? [];
  }
}
