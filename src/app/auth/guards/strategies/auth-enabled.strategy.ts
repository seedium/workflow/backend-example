import { ExecutionContext, Injectable } from '@nestjs/common';
import { httpAssert, isError } from '@utils';
import { ApplicationFastifyRequest } from '@modules/core/interfaces';
import { CompletedUser } from '@app/user/interfaces';
import { AccountUserService } from '@app/relations/account-user/services';
import { AuthenticationService, JwtService } from '../../services';
import { AuthStrategy } from '../../interfaces';
import { NotAuthenticatedRequestException } from '../../exceptions';

@Injectable()
export class AuthEnabledStrategy implements AuthStrategy {
  constructor(
    private readonly jwtService: JwtService,
    private readonly accountUserService: AccountUserService,
    private readonly authenticationService: AuthenticationService,
  ) {}

  public async execute(context: ExecutionContext): Promise<boolean> {
    const req = context
      .switchToHttp()
      .getRequest<ApplicationFastifyRequest<CompletedUser>>();
    const tokenOrError = this.jwtService.extractJwtTokenFromRequest(req);
    httpAssert(
      !isError(tokenOrError),
      new NotAuthenticatedRequestException((tokenOrError as Error).message),
    );
    const userOrError = await this.authenticationService.retrieveUserByToken(
      tokenOrError,
    );
    httpAssert(
      !isError(userOrError),
      new NotAuthenticatedRequestException((userOrError as Error).message),
    );
    req.user = userOrError;
    return true;
  }
}
