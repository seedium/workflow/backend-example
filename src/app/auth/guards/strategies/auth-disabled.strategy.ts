import { ExecutionContext, Injectable } from '@nestjs/common';
import { ApplicationFastifyRequest } from '@modules/core/interfaces';
import { ResourceMissingException } from '@lib/exceptions/api';
import { UserService } from '@app/user/services';
import { User } from '@app/user/interfaces';
import { httpAssert } from '@utils';
import { AuthStrategy } from '../../interfaces';

@Injectable()
export class AuthDisabledStrategy implements AuthStrategy {
  constructor(private readonly userService: UserService) {}

  public async execute(context: ExecutionContext): Promise<boolean> {
    const req = context
      .switchToHttp()
      .getRequest<ApplicationFastifyRequest<User>>();

    const idUser = this.extractUserId(req);
    httpAssert(
      idUser,
      new Error(
        'Authentication is disabled in your feature flags. Please, to using application with disabled authentication,  send `example-dev-user-id` header with valid id user with valid guest token session',
      ),
    );
    const user = await this.userService.retrieve(idUser);
    httpAssert(
      user,
      new ResourceMissingException(`User with id "${idUser}" not found`),
    );
    req.user = user;
    return true;
  }

  private extractUserId(req: ApplicationFastifyRequest<User>): string | null {
    return req.headers['example-dev-user-id'] ?? null;
  }
}
