import {
  CanActivate,
  ExecutionContext,
  Inject,
  Injectable,
} from '@nestjs/common';
import { FastifyRequest } from 'fastify';
import { auth0Config, Auth0Config } from '@config/auth0.config';
import { httpAssert } from '@utils';
import { ApiKeyRequiredException } from '../exceptions';

@Injectable()
export class Auth0KeyAuthenticationGuard implements CanActivate {
  constructor(
    @Inject(auth0Config.KEY)
    private readonly a0c: Auth0Config,
  ) {}
  public canActivate(context: ExecutionContext): boolean {
    const apiKey = this.extractApiKeyFromRequest(
      context.switchToHttp().getRequest(),
    );
    httpAssert(apiKey, new ApiKeyRequiredException());
    httpAssert(
      this.a0c.hooks.apiKey === apiKey,
      new ApiKeyRequiredException(`API key is invalid`),
    );
    return true;
  }

  private extractApiKeyFromRequest(
    req: FastifyRequest<{ Headers: { 'x-api-key'?: string } }>,
  ): string | null {
    return req.headers['x-api-key'] ?? null;
  }
}
