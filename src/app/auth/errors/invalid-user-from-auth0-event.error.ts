export class InvalidUserFromAuth0EventError extends Error {
  constructor(event?: string) {
    super(`User on [${event || 'unknown'}] doesn't have required fields`);
  }
}
