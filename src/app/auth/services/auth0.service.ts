import { Inject, Injectable } from '@nestjs/common';
import { ManagementClient, User } from 'auth0';
import { auth0Config, Auth0Config } from '@config/auth0.config';
import { featuresConfig, FeaturesConfig } from '@config/features.config';
import { Auth0AppMetadata, Auth0UserMetadata } from '../interfaces';

@Injectable()
export class Auth0Service {
  private readonly managementClient: ManagementClient<
    Auth0AppMetadata,
    Auth0UserMetadata
  >;
  constructor(
    @Inject(auth0Config.KEY)
    private readonly a0c: Auth0Config,
    @Inject(featuresConfig.KEY)
    private readonly fc: FeaturesConfig,
  ) {
    if (!this.fc.auth || !this.a0c.management.domain) {
      return;
    }
    this.managementClient = new ManagementClient({
      domain: this.a0c.management.domain,
      clientId: this.a0c.management.clientId,
      clientSecret: this.a0c.management.clientSecret,
      audience: this.a0c.management.audience,
      scope: this.a0c.management.scopes.join(' '),
    });
  }
  public async getUser(
    idAuth0: string,
  ): Promise<User<Auth0AppMetadata, Auth0UserMetadata>> {
    return await this.handleAuthError(() =>
      this.managementClient.getUser({
        id: idAuth0,
      }),
    );
  }
  private async handleAuthError<T>(cb: () => Promise<T>): Promise<T> {
    try {
      return await cb();
    } catch (err) {
      throw new Error(this.parseErrorMessage(err));
    }
  }
  private parseErrorMessage(err: unknown): string {
    if (err instanceof Error) {
      return err.message;
    }
    try {
      const parsedResult = JSON.parse((err as Error).message);
      return parsedResult.error_description;
    } catch {
      return (err as Error).message;
    }
  }
}
