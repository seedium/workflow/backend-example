import S from 'fluent-json-schema';
import { auth0ActionEventUserDtoSchema } from './auth0-action-event-user-dto.schema';

export const auth0ContextDtoSchema = S.object()
  .additionalProperties(false)
  .prop('tenant', S.string())
  .prop('clientID', S.string())
  .prop('clientName', S.string())
  .prop('clientMetadata', S.object())
  .prop('connection', S.string())
  .prop('connectionStrategy', S.string())
  .prop('connectionID', S.string())
  .prop('connectionOptions', S.object())
  .prop('connectionMetadata', S.object())
  .prop('samlConfiguration', S.object())
  .prop('jwtConfiguration', S.object())
  .prop('protocol', S.string())
  .prop(
    'stats',
    S.object().additionalProperties(false).prop('loginsCount', S.number()),
  )
  .prop('sessionID', S.string())
  .prop(
    'request',
    S.object()
      .additionalProperties(false)
      .prop(
        'geoip',
        S.object()
          .additionalProperties(false)
          .prop('country_code', S.string())
          .prop('country_code3', S.string())
          .prop('country_name', S.string())
          .prop('city_name', S.string())
          .prop('latitude', S.number())
          .prop('longitude', S.number())
          .prop('time_zone', S.string())
          .prop('continent_code', S.string()),
      ),
  );

export const auth0RequestDtoSchema = S.object()
  .additionalProperties(false)
  .prop(
    'geoip',
    S.object()
      .additionalProperties(false)
      .prop('cityName', S.string())
      .prop('countryName', S.string())
      .prop('latitude', S.number())
      .prop('longitude', S.number())
      .prop('timeZone', S.string()),
  )
  .prop('ip', S.string())
  .prop('language', S.string());

export const preSignupBodySchema = S.object()
  .additionalProperties(false)
  .prop('user', auth0ActionEventUserDtoSchema)
  .prop('request', auth0RequestDtoSchema);

export const postSignupBodySchema = S.object()
  .additionalProperties(false)
  .prop('user', auth0ActionEventUserDtoSchema)
  .prop('request', auth0RequestDtoSchema);

export const changePasswordBodySchema = S.object()
  .additionalProperties(false)
  .required(['new_password'])
  .prop('new_password', S.string());

export const permissionsDtoSchema = S.array().items(S.string());
