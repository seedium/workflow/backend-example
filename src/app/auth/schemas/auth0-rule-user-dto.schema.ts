import S from 'fluent-json-schema';
import { auth0UserIdentity } from './auth0-user-general-dtos.schema';

export const auth0RuleUserDtoSchema = S.object()
  .additionalProperties(false)
  .prop('_id', S.string())
  .prop('clientID', S.string())
  .prop('created_at', S.string())
  .prop('email', S.string())
  .prop('email_verified', S.boolean())
  .prop('family_name', S.string())
  .prop('given_name', S.string())
  .prop('identities', S.array().items(auth0UserIdentity))
  .prop('locale', S.string())
  .prop('name', S.string())
  .prop('nickname', S.string())
  .prop('picture', S.string())
  .prop('updated_at', S.string())
  .prop('user_id', S.string())
  .prop('global_client_id', S.string())
  .prop('persistent', S.object())
  .prop('app_metadata', S.object());
