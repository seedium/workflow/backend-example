import { UseGuards, Post, Body } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiExcludeController } from '@nestjs/swagger';
import { RestController, RestMethod } from '@modules/core/decorators';
import { Metadata, MetadataContainer } from '@modules/core/metadata';
import { ResourceMissingException } from '@lib/exceptions/api';
import { UserEmailAlreadyExistsError } from '@app/user/errors';
import { UserEmailAlreadyExistsException } from '@app/user/exceptions';
import { httpAssert, isError } from '@utils';
import { AuthService } from '../services';
import { Auth0KeyAuthenticationGuard } from '../guards';
import { InvalidUserFromAuth0EventError } from '../errors';
import { InvalidUserFromAuth0EventException } from '../exceptions';
import {
  Auth0EventRequestDto,
  Auth0EventUserPreSignupDto,
  Auth0EventUserPostSignupDto,
} from '../interfaces';
import {
  postSignupBodySchema,
  preSignupBodySchema,
} from '../schemas/auth-controller.schema';

@ApiExcludeController(true)
@ApiTags('Auth')
@RestController('auth')
export class Auth0Controller {
  constructor(private readonly authService: AuthService) {}

  @Post('pre_signup')
  @ApiOperation({
    summary: 'Used by auth0 to check if a signup is allowed',
  })
  @RestMethod(
    {
      scopes: [],
      statusCode: 204,
      body: preSignupBodySchema,
    },
    UseGuards(Auth0KeyAuthenticationGuard),
  )
  public async preSignUp(
    @Body('user') auth0EventUserDto: Auth0EventUserPreSignupDto,
    @Body('request') authEventRequest: Auth0EventRequestDto,
    @Metadata() metadata: MetadataContainer,
  ): Promise<void> {
    const userOrError = await this.authService.databasePreSignUp(
      authEventRequest,
      auth0EventUserDto,
      metadata,
    );
    httpAssert(
      !(userOrError instanceof InvalidUserFromAuth0EventError),
      new InvalidUserFromAuth0EventException(),
    );
    httpAssert(
      !(userOrError instanceof UserEmailAlreadyExistsError),
      new UserEmailAlreadyExistsException(auth0EventUserDto.email),
    );
    httpAssert(!isError(userOrError), userOrError);
  }

  @Post('post_signup')
  @ApiOperation({
    summary: 'Used by auth0 after a signup was successful',
  })
  @RestMethod(
    {
      scopes: [],
      statusCode: 204,
      body: postSignupBodySchema,
    },
    UseGuards(Auth0KeyAuthenticationGuard),
  )
  public async postSignUp(
    @Body('user') auth0EventUserDto: Auth0EventUserPostSignupDto,
    @Body('request') authEventRequest: Auth0EventRequestDto,
    @Metadata() metadata: MetadataContainer,
  ): Promise<void> {
    const userOrError = await this.authService.databasePostSignUp(
      authEventRequest,
      auth0EventUserDto,
      metadata,
    );
    httpAssert(userOrError, new ResourceMissingException());
    httpAssert(
      !(userOrError instanceof InvalidUserFromAuth0EventError),
      new InvalidUserFromAuth0EventException(),
    );
    httpAssert(!isError(userOrError), userOrError);
  }
}
