/* eslint-disable @typescript-eslint/no-empty-interface */

export interface Auth0AppMetadata {}

export interface Auth0UserMetadata {}

export interface Auth0UserIdentity {
  connection: string;
  user_id: string;
  provider: string;
  isSocial: boolean;
}

export interface Auth0RuleUserDto {
  _id: string;
  clientID: string;
  created_at: string;
  email: string;
  email_verified: boolean;
  family_name?: string;
  given_name?: string;
  identities: Auth0UserIdentity[];
  locale: string;
  name?: string;
  nickname?: string;
  picture?: string;
  updated_at: string;
  user_id: string;
  global_client_id: string;
  persistent: Record<string, unknown>;
  app_metadata: Auth0AppMetadata;
}

export interface Auth0EventUserPreSignupDto {
  app_metadata: Auth0AppMetadata;
  user_metadata: Auth0UserMetadata;
  email?: string;
  family_name?: string;
  given_name?: string;
  name?: string;
  nickname?: string;
  phone_number?: string;
  picture?: string;
}

export interface Auth0EventUserPostSignupDto {
  app_metadata: Auth0AppMetadata;
  user_metadata: Auth0UserMetadata;
  email?: string;
  email_verified: boolean;
  family_name: string;
  given_name: string;
  name?: string;
  nickname?: string;
  phone_number?: string;
  phone_verified?: boolean;
  picture?: string;
  user_id: string;
  username?: string;
}

export interface Auth0RuleContextDto {
  tenant: string;
  clientID: string;
  clientName: string;
  clientMetadata: Record<string, unknown>;
  connection: string;
  connectionStrategy: string;
  connectionID: string;
  connectionOptions: Record<string, unknown>;
  connectionMetadata: Record<string, unknown>;
  samlConfiguration: Record<string, unknown>;
  jwtConfiguration: Record<string, unknown>;
  protocol: string;
  stats: {
    loginsCount: number;
  };
  sessionID: string;
  request: {
    geoip: {
      country_code?: string;
      country_code3?: string;
      country_name?: string;
      city_name?: string;
      latitude?: number;
      longitude?: number;
      time_zone?: string;
      continent_code?: string;
    };
  };
}

export interface Auth0EventRequestDto {
  geoip: {
    cityName?: string;
    countryName?: string;
    latitude?: number;
    longitude?: number;
    timeZone?: string;
  };
  language?: string;
  ip: string;
}
