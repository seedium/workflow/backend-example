import { Module } from '@nestjs/common';
import { KnexModule } from '@modules/knex';
import { CqrsModule } from '@modules/cqrs/cqrs.module';
import { TransactionService } from './services';

@Module({
  imports: [KnexModule.forRoot(), CqrsModule.forRoot()],
  providers: [TransactionService],
  exports: [TransactionService],
})
export class TransactionModule {}
