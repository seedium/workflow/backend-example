import { Module, OnModuleInit } from '@nestjs/common';
import addFormats from 'ajv-formats';
import addKeywords from 'ajv-keywords';
import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
import {
  AjvValidator,
  ValidationModule,
  JsonSchemaSerializerInterceptor,
} from 'nestjs-validation';
import {
  HttpExceptionFilter,
  AuthExceptionFilter,
  GlobalExceptionFilter,
  NotFoundExceptionFilter,
  ValidationExceptionFilter,
} from './filters';
import { RequestLogger, TracingProvider } from './providers';
import { RequestInterceptorInterceptor } from './interceptors';

@Module({
  imports: [ValidationModule.forFeature()],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: JsonSchemaSerializerInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: RequestInterceptorInterceptor,
    },
    {
      provide: APP_FILTER,
      useClass: GlobalExceptionFilter,
    },
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
    {
      provide: APP_FILTER,
      useClass: ValidationExceptionFilter,
    },
    {
      provide: APP_FILTER,
      useClass: AuthExceptionFilter,
    },
    {
      provide: APP_FILTER,
      useClass: NotFoundExceptionFilter,
    },
    TracingProvider,
    RequestLogger,
  ],
})
export class CoreModule implements OnModuleInit {
  constructor(private readonly ajvValidator: AjvValidator) {}

  public onModuleInit(): void {
    addFormats(this.ajvValidator.ajv, ['email', 'url']);
    addKeywords(this.ajvValidator.ajv, ['transform']);
  }
}
