import { TRACEPARENT_REGEXP } from '@sentry/tracing';
import { isString } from '@utils';

export class SentryTracer {
  static extractParentTrace(sentryTrace: unknown): Record<string, unknown> {
    if (!sentryTrace) {
      return {};
    }
    if (!isString(sentryTrace)) {
      return {};
    }
    const matches = sentryTrace.match(TRACEPARENT_REGEXP);
    if (!matches) {
      return {};
    }
    let parentSampled: boolean | undefined;
    if (matches[3] === '1') {
      parentSampled = true;
    } else if (matches[3] === '0') {
      parentSampled = false;
    }
    return {
      traceId: matches[1],
      parentSampled,
      parentSpanId: matches[2],
    };
  }
}
