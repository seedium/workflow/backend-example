import {
  Catch,
  ExceptionFilter,
  ArgumentsHost,
  ForbiddenException,
} from '@nestjs/common';
import { FastifyRequest, FastifyReply } from 'fastify';
import { NotAuthenticatedRequestException } from '@app/auth/exceptions';

@Catch(ForbiddenException)
export class AuthExceptionFilter
  implements ExceptionFilter<ForbiddenException>
{
  catch(_: ForbiddenException, host: ArgumentsHost): void {
    const ctx = host.switchToHttp();
    const res = ctx.getResponse<FastifyReply>();
    const req = ctx.getRequest<FastifyRequest>();
    const notAuthenticatedException = new NotAuthenticatedRequestException(
      'You are not authenticated or requested resource is forbidden for you',
    );

    res.status(notAuthenticatedException.status).send({
      ...notAuthenticatedException,
      requestId: req.id,
      message: notAuthenticatedException.message,
    });
  }
}
