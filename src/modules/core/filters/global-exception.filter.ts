import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  ServiceUnavailableException,
  Logger,
} from '@nestjs/common';
import { FastifyError, FastifyReply } from 'fastify';
import * as Sentry from '@sentry/node';
import { HttpArgumentsHost } from '@nestjs/common/interfaces';
import { Scope } from '@sentry/types';
import { COMMAND_BUST_HOST_TYPE } from '@modules/cqrs/cqrs.constants';
import { ICommand } from '@modules/cqrs';
import { ApplicationFastifyRequest } from '@modules/core/interfaces';
import {
  ApiBaseException,
  ServerException,
  ServerUnavailableException,
  UnsupportedMediaTypeException,
} from '@lib/exceptions/api';

@Catch(Error, ServiceUnavailableException)
export class GlobalExceptionFilter implements ExceptionFilter<Error> {
  private readonly _logger = new Logger(GlobalExceptionFilter.name);
  catch(exception: Error, host: ArgumentsHost): void {
    if (host.getType() === 'http') {
      this.handleHttpException(exception, host.switchToHttp());
    } else if ((host.getType() as string) === COMMAND_BUST_HOST_TYPE) {
      this.handleCommandBusException(exception, host);
    } else {
      this._logger.warn(`Unknown host type "${host.getType()}"`);
    }
  }
  private handleHttpException(exception: Error, ctx: HttpArgumentsHost): void {
    const res = ctx.getResponse<FastifyReply>();
    const req = ctx.getRequest<ApplicationFastifyRequest>();
    const apiException = this.transformExceptionToApiException(exception);

    if (apiException instanceof ServerException) {
      this.processHttpCriticalError(req, exception);
    }

    res.status(apiException.status).send({
      ...apiException,
      requestId: req.id,
      message: apiException.message,
    });
  }
  private handleCommandBusException(
    exception: Error,
    host: ArgumentsHost,
  ): void {
    this.processCommandBusCriticalError(
      exception,
      host.getArgByIndex(0),
      host.getArgByIndex(1),
    );
  }
  private transformExceptionToApiException(exception: Error): ApiBaseException {
    if (exception instanceof ServiceUnavailableException) {
      return new ServerUnavailableException(exception.message);
    } else if (
      this.isFastifyError(exception) &&
      this.isFastifyUnsupportedMediaType(exception)
    ) {
      return new UnsupportedMediaTypeException(exception.message);
    } else {
      return new ServerException(exception);
    }
  }
  private isFastifyError(error: Error): error is FastifyError {
    return 'code' in error;
  }
  private isFastifyUnsupportedMediaType(fastifyError: FastifyError): boolean {
    return fastifyError.code === 'FST_ERR_CTP_INVALID_MEDIA_TYPE';
  }
  private processHttpCriticalError(
    req: ApplicationFastifyRequest,
    exception: Error,
  ): void {
    this._logger.error(
      {
        ...exception,
        traceId: req.metadata.traceId,
        msg: exception.message,
        req: {
          headers: req.headers,
          query: req.query,
          body: req.body,
          params: req.params,
          ip: req.ip,
        },
      },
      exception.stack,
    );
    this.sendToSentry(exception, req.sentryScope);
  }
  private processCommandBusCriticalError(
    exception: Error,
    command: ICommand,
    sentryScope: Scope,
  ): void {
    this._logger.error(
      {
        ...exception,
        traceId: command.traceId,
        commandId: command.id,
        commandName: command.constructor.name,
        command,
      },
      exception.stack,
    );
    this.sendToSentry(exception, sentryScope);
  }
  private sendToSentry(exception: Error, sentryScope: Scope): void {
    const transaction = sentryScope.getTransaction();
    if (transaction) {
      transaction.setStatus('internal_error');
    }
    Sentry.captureException(exception, () => sentryScope);
  }
}
