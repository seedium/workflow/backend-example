import { Injectable } from '@nestjs/common';
import { EventBusTransaction } from '../lib';

@Injectable()
export class EventBusTransactionService {
  public transaction(): EventBusTransaction {
    return new EventBusTransaction();
  }
}
