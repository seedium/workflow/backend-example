import { Injectable } from '@nestjs/common';
import { Knex } from 'knex';
import { InjectKnexManager } from '@modules/knex';
import { KnexTransactionConfig } from '../interfaces';

@Injectable()
export class KnexTransactionService {
  constructor(@InjectKnexManager() private readonly _knex: Knex) {}
  public async transaction(
    config?: KnexTransactionConfig,
  ): Promise<Knex.Transaction> {
    const transaction = await this._knex.transaction(config);
    if (config?.deferred && config.deferred.length) {
      await transaction.raw(
        `set constraints ${config.deferred.join(',')} deferred;`,
      );
    }
    return transaction;
  }
}
