import { Controller, Get } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiExcludeEndpoint } from '@nestjs/swagger';
import {
  HealthCheck,
  HealthCheckService,
  HealthCheckResult,
  MemoryHealthIndicator,
} from '@nestjs/terminus';
import { getBytesFromFriendly } from '@utils';
import { DatabaseHealthIndicator } from '@modules/knex';

@ApiTags('Health indicator')
@Controller('health')
export class HealthCheckController {
  constructor(
    private readonly healthCheckService: HealthCheckService,
    private readonly memoryHealthIndicator: MemoryHealthIndicator,
    private readonly _databaseHealthIndicator: DatabaseHealthIndicator,
  ) {}

  @Get('readiness')
  @ApiOperation({
    summary: 'Returns the status of the application',
  })
  @ApiExcludeEndpoint(true)
  @HealthCheck()
  public async readiness(): Promise<HealthCheckResult> {
    return this.isApplicationHealthy();
  }

  @Get('liveness')
  @ApiOperation({
    summary: 'Returns the status of the application',
  })
  @ApiExcludeEndpoint(true)
  @HealthCheck()
  public async liveness(): Promise<HealthCheckResult> {
    return this.isApplicationHealthy();
  }

  private isApplicationHealthy(): Promise<HealthCheckResult> {
    return this.healthCheckService.check([
      () =>
        this.memoryHealthIndicator.checkHeap(
          'memory_heap',
          getBytesFromFriendly(200, 'Mb'),
        ),
      () =>
        this.memoryHealthIndicator.checkRSS(
          'memory_rss',
          getBytesFromFriendly(300, 'Mb'),
        ),
      () => this._databaseHealthIndicator.isHealthy(),
    ]);
  }
}
