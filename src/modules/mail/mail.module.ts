import { Module } from '@nestjs/common';
import { MailService, SendgridService } from './services';

@Module({
  providers: [MailService, SendgridService],
  exports: [MailService],
})
export class MailModule {}
