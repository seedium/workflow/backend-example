import { SendgridSendOptions } from './sendgrid-send-options.interface';

export type SendOptions = SendgridSendOptions;
