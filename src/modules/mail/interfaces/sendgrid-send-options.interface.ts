import { MailDataRequired } from '@sendgrid/mail';

type SendgridSendBaseOptions = Omit<
  MailDataRequired,
  'to' | 'from' | 'text' | 'html' | 'templateId' | 'dynamicTemplateData'
>;

type TextSendgridSendOptions = SendgridSendBaseOptions & { text: string };
type HtmlSendgridSendOptions = SendgridSendBaseOptions & { html: string };
type TemplateSendgridSendOptions = SendgridSendBaseOptions & {
  templateId: string;
  dynamicTemplateData: Record<string, unknown>;
};

export type SendgridSendOptions =
  | TextSendgridSendOptions
  | HtmlSendgridSendOptions
  | TemplateSendgridSendOptions;
