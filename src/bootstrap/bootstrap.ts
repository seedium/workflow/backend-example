import { NestApplicationContextOptions } from '@nestjs/common/interfaces/nest-application-context-options.interface';
import { NestFastifyApplication } from '@nestjs/platform-fastify';
import { appConfig, AppConfig } from '@config/app.config';
import { setupApplication } from './setup-application';
import { setupSwagger } from './setup-swagger';

export const bootstrap = async (
  options?: NestApplicationContextOptions,
): Promise<NestFastifyApplication> => {
  const app = await setupApplication(options);
  const config = app.get<AppConfig>(appConfig.KEY);
  setupSwagger(app, {
    version: config.version || '0.0.0',
    environment: config.environment,
  });
  await app.listen(config.port, config.address);
  return app;
};
