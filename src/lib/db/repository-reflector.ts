import { Type } from '@nestjs/common';
import { reflect } from '@utils';
import {
  EntityRepositoryOptions,
  ExpandableOptions,
  FieldTransformer,
} from './interfaces';
import { PostgresRepository } from './postgres-repository';
import { ENTITY_REPOSITORY_OPTIONS } from './db.constants';

export class RepositoryReflector<TRecord, TResult> {
  private readonly _repositoryOptions: EntityRepositoryOptions;
  public get table(): string {
    return this._repositoryOptions.name;
  }
  public get expandable(): ExpandableOptions {
    return this._repositoryOptions.expandable ?? {};
  }
  public get transform(): Partial<Record<string, FieldTransformer>> | null {
    return this._repositoryOptions.transform ?? null;
  }
  public get prefix(): string | undefined {
    return this._repositoryOptions.prefix;
  }
  constructor(
    repositoryClassOrConstructor:
      | Function
      | Type<PostgresRepository<TRecord, TResult>>,
  ) {
    this._repositoryOptions = this.reflectRepositoryOptions(
      repositoryClassOrConstructor,
    );
  }
  private reflectRepositoryOptions(
    reflectObject: Function | Type<PostgresRepository<TRecord, TResult>>,
  ): EntityRepositoryOptions {
    const entityRepositoryOptions = reflect<EntityRepositoryOptions>(
      reflectObject,
      ENTITY_REPOSITORY_OPTIONS,
    );
    if (!entityRepositoryOptions) {
      throw new Error(
        `"${reflectObject.name}" doesn't have decorator @EntityRepository`,
      );
    }
    return entityRepositoryOptions;
  }
}
