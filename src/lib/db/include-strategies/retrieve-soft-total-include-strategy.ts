import { IListOptionsCreated, HardResourceObject } from '../interfaces';
import { SoftTotalCountIncludeStrategy } from './soft-total-count-include.strategy';

export const retrieveSoftTotalIncludeStrategy = <
  TRecord extends HardResourceObject,
  TResult extends HardResourceObject,
>(
  options?: IListOptionsCreated,
): [SoftTotalCountIncludeStrategy<TRecord, TResult>] | [] => {
  if (options?.include?.includes('total_count')) {
    return [new SoftTotalCountIncludeStrategy<TRecord, TResult>(options)];
  }
  return [];
};
