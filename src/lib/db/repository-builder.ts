import { Knex } from 'knex';
import { addPrefixColumn, sanitizeFilterOptions } from './utils';
import {
  IComplexDateFilter,
  IDateFilter,
  FactoryRawBuilder,
  BuildFilterQueryOptions,
  IPaginationBuilder,
  BuildDateFilterQueryOptions,
  BuildSortQueryOptions,
} from './interfaces';

export class RepositoryBuilder<TRecord, TResult> {
  constructor(
    private readonly _pagination: IPaginationBuilder<TRecord, TResult>,
  ) {}
  public buildFilterQuery(
    queryBuilder: Knex.QueryBuilder<TRecord, TResult>,
    factoryRawBuilder: FactoryRawBuilder<TRecord, TResult>,
    filterObject: Record<string, unknown>,
    { strategies = [], alias }: BuildFilterQueryOptions<TRecord, TResult> = {},
  ): void {
    /*
     * TODO We can optimize this part by trusting validation layer.
     *  So, by building the right schema where we can filter number and string values we can expect do not fall.
     * */
    const sanitizedFilter = sanitizeFilterOptions(filterObject);
    queryBuilder.where(this.appendAliasToFilterColumns(sanitizedFilter, alias));
    if (strategies.length > 0) {
      strategies.forEach((filterStrategy) =>
        queryBuilder.andWhereRaw(
          filterStrategy.inline(factoryRawBuilder()) as Knex.Raw<TResult>,
        ),
      );
    }
  }
  public buildPaginationQuery(
    queryBuilder: Knex.QueryBuilder<TRecord, TResult>,
    limit?: number,
    ...args: unknown[]
  ): void {
    return this._pagination.buildQuery(queryBuilder, limit, ...args);
  }
  public buildSortQuery(
    queryBuilder: Knex.QueryBuilder<TRecord, TResult>,
    sort: string[],
    options?: BuildSortQueryOptions,
  ): void {
    sort.forEach((property) => {
      const [direction, column] = this.getSortDirection(property);
      queryBuilder.orderBy(addPrefixColumn(column, options?.alias), direction);
    });
  }
  public buildDateFilterQuery(
    queryBuilder: Knex.QueryBuilder<TRecord, TResult>,
    dateFilter?: IDateFilter,
    options?: BuildDateFilterQueryOptions,
  ): void {
    if (!dateFilter) {
      return;
    }
    if (this.isDateFilterTimestamp(dateFilter)) {
      queryBuilder.where(
        addPrefixColumn('created_at', options?.alias),
        dateFilter,
      );
    } else {
      this.buildComplexDateFilterQuery(queryBuilder, dateFilter, options);
    }
  }
  protected getSortDirection(property: string): ['asc' | 'desc', string] {
    const sortSymbol = property[0];
    if (sortSymbol === '+') {
      return ['asc', property.slice(1, property.length)];
    } else if (sortSymbol === '-') {
      return ['desc', property.slice(1, property.length)];
    } else {
      return ['asc', property];
    }
  }
  protected buildComplexDateFilterQuery(
    queryBuilder: Knex.QueryBuilder<TRecord, TResult>,
    dateFilter: IComplexDateFilter,
    options?: BuildDateFilterQueryOptions,
  ): void {
    Object.entries(dateFilter).forEach(([operator, value]) =>
      this.getQueryForDateOperator(
        queryBuilder,
        operator as 'gt' | 'gte' | 'lt' | 'lte',
        value,
        options?.alias,
      ),
    );
  }
  private appendAliasToFilterColumns(
    filter: Record<string, unknown>,
    alias?: string,
  ): Record<string, unknown> {
    return Object.fromEntries(
      Object.entries(filter).map(([column, value]) => [
        addPrefixColumn(column, alias),
        value,
      ]),
    );
  }
  private isDateFilterTimestamp(dateFilter: IDateFilter): dateFilter is number {
    return typeof dateFilter === 'number';
  }
  private getQueryForDateOperator(
    queryBuilder: Knex.QueryBuilder<TRecord, TResult>,
    operator: 'gt' | 'gte' | 'lt' | 'lte',
    value: number,
    alias?: string,
  ): void {
    const createdAtColumnName = addPrefixColumn('created_at', alias);
    if (operator === 'gt') {
      queryBuilder.where(createdAtColumnName, '>', value);
    } else if (operator === 'gte') {
      queryBuilder.where(createdAtColumnName, '>=', value);
    } else if (operator === 'lt') {
      queryBuilder.where(createdAtColumnName, '<', value);
    } else if (operator === 'lte') {
      queryBuilder.where(createdAtColumnName, '<=', value);
    }
  }
}
