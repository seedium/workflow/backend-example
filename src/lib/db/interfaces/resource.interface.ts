import { Nullable } from '@lib/interfaces';
import { IAccountResourceModel } from '@lib/db';

export type ID = string;

export interface HardResourceObject {
  id: ID;
  created_at: number;
  updated_at: number;
}

export interface IResourceObject extends HardResourceObject {
  deleted_at: Nullable<number>;
}

export type OmitDefaultResourceFields<T> = T extends IResourceObject
  ? Omit<T, 'id' | 'created_at' | 'updated_at' | 'deleted_at'>
  : T;

export interface ICreateTimestamp {
  created_at: number;
  updated_at: number;
}

export interface IUpdateTimestamp {
  updated_at: ICreateTimestamp['updated_at'];
}

export interface ISoftTimestampResource extends ICreateTimestamp {
  deleted_at: Nullable<number>;
}

export type ITimestampResourceObject = ICreateTimestamp;

export type OmitTimestampResourceObject<T extends ITimestampResourceObject> =
  Omit<T, 'created_at' | 'updated_at'>;

export type OmitDefaultAccountResourceFields<T extends IAccountResourceModel> =
  Omit<OmitDefaultResourceFields<T>, 'account'>;

export type OmitDefaultHardResourceFields<T> = T extends HardResourceObject
  ? Omit<T, 'id' | 'created_at' | 'updated_at'>
  : T;
