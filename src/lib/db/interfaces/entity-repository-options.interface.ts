import { Type, ForwardReference } from '@nestjs/common';
import {
  ExpandRepositoryStrategyOptions,
  SoftAccountRepository,
  SoftRepository,
  Repository,
} from '@lib/db';

export type TypeRepository = Type<
  // TODO need to avoid any here
  // eslint-disable-next-line
  | SoftAccountRepository<any, any>
  // eslint-disable-next-line
  | SoftRepository<any, any>
  // eslint-disable-next-line
  | Repository<any, any>
>;

export interface ListExpandableOptions
  extends Partial<ExpandRepositoryStrategyOptions> {
  foreignField?: string;
  repository:
    | TypeRepository
    | ForwardReference<() => TypeRepository>
    | undefined;
}

export type ExpandableOptions = Record<
  string,
  | ListExpandableOptions
  | TypeRepository
  | ForwardReference<() => TypeRepository>
  | undefined
>;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type FieldTransformer<TValue = any, TResult = unknown> = (
  value: TValue,
) => TResult;

export interface EntityRepositoryOptions {
  name: string;
  prefix?: string;
  expandable?: ExpandableOptions;
  transform?: Partial<Record<string, FieldTransformer>>;
}
