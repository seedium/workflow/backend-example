import { Knex } from 'knex';
import { IDataOptions, IDateFilter } from './list.interface';
import { FilterInlineStrategy } from './inline.strategy';
import { IncludeStrategy } from './include-strategy.interface';

export type RepositoryDataOptions = Omit<IDataOptions, 'include'>;

/*
 * Can't just to remove property `include` from `IListOptionsCreated` type,
 * cuz this property comes from inheritance `IDataOptions` type and standard type `Omit` not working here.
 * So, just copy all properties from original `IListOptionsCreated` type
 * */
export interface RepositoryListOptionsCreated extends RepositoryDataOptions {
  page?: number;
  sort?: string[];
  limit?: number;
  created?: IDateFilter;
  [filterColumn: string]: string | number | unknown;
}

export interface RepositoryFindOptions extends RepositoryDataOptions {
  [filterColumn: string]: string | number | unknown;
}

export interface RepositoryDatabaseOptions {
  transaction?: Knex.Transaction;
}

export type IncludeStrategies<TRecord, TResult> = IncludeStrategy<
  TRecord,
  TResult,
  unknown
>[];

export type FilterStrategies<TRecord, TResult> = FilterInlineStrategy<
  TRecord,
  TResult
>[];

export interface BuildFilterQueryOptions<TRecord, TResult> {
  alias?: string;
  strategies?: FilterStrategies<TRecord, TResult>;
}

export interface BuildDateFilterQueryOptions {
  alias?: string;
}

export interface BuildSortQueryOptions {
  alias?: string;
}

export interface AliasableRepositoryDatabaseOptions {
  alias?: string;
}

export interface SelectRepositoryDatabaseOptions<TRecord, TResult>
  extends RepositoryDatabaseOptions,
    AliasableRepositoryDatabaseOptions {
  include?: IncludeStrategies<TRecord, TResult>;
  filter?: FilterStrategies<TRecord, TResult>;
}
