import { O } from 'ts-toolbelt';
import { ID, IResourceObject } from '../interfaces';

export type PartialBy<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>;

export type RequiredBy<T, K extends keyof T> = T & Required<Pick<T, K>>;

export type ForeignRef<T> = ID | T;

export type Expand<
  T extends IResourceObject,
  TExpand extends O.Object,
> = O.Overwrite<T, TExpand>;
