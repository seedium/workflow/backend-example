import { IRepository } from './repository.interface';
import { RepositoryDatabaseOptions } from './database-options.interface';
import { IList } from './list.interface';

export type ExpandStrategy<
  T extends Record<string, unknown> = Record<string, unknown>,
> = (
  repository: IRepository,
  record: T,
  options: {
    foreignField: string;
    localField: string;
    expand: string[];
    databaseOptions?: RepositoryDatabaseOptions;
  },
) => Promise<unknown | IList<unknown>>;

export interface ExpandRepositoryStrategyOptions {
  retrieveStrategy: ExpandStrategy;
  listStrategy: ExpandStrategy;
}

export interface ExpandRepositoryOptions
  extends ExpandRepositoryStrategyOptions {
  isList: boolean;
  localField: string;
  foreignField: string;
  repository: IRepository;
}
