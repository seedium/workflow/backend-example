export interface ScopesOptions {
  checkAllScopes?: boolean;
}

export class ScopeService {
  public verify(
    expectedScopes: string[],
    actualScopes: string[],
    options?: ScopesOptions,
  ): boolean {
    if (!expectedScopes || !expectedScopes.length) {
      return true;
    }
    if (options?.checkAllScopes) {
      return expectedScopes.every((scope) => actualScopes.includes(scope));
    }
    return expectedScopes.some((scope) => actualScopes.includes(scope));
  }
}
