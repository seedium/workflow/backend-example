import { BaseException } from '../base.exception';

export abstract class ApiBaseException extends BaseException {
  public type = 'api_error';
}
