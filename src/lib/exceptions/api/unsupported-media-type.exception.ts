import { ApiBaseException } from '@lib/exceptions/api/api-base.exception';

export class UnsupportedMediaTypeException extends ApiBaseException {
  public code = 'unsupported_media_type';
  public status = 415;
  constructor(message = 'Unsupported Media Type') {
    super(message);
  }
}
