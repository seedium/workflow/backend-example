import { BaseException } from '../base.exception';

export abstract class ValidationBaseException extends BaseException {
  public type = 'validation_error';
}
