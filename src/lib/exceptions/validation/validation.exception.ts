import { ValidationBaseException } from './validation-base.exception';

export class ValidationException extends ValidationBaseException {
  code = 'validation_failed';
  status = 400;
  constructor(message?: string) {
    super(
      message ||
        `Some of provided values are invalid. Please fix them and try again`,
    );
  }
}
