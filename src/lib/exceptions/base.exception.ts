export abstract class BaseException extends Error {
  public abstract type: string;
  public abstract code: string;
  public abstract status: number;

  constructor(message?: string) {
    super(message || 'Unknown error');
  }
}
