import { ApiParam } from '@nestjs/swagger';
import { JSONSchema } from 'fluent-json-schema';
import {
  ReferenceObject,
  SchemaObject,
} from '@nestjs/swagger/dist/interfaces/open-api-spec.interface';
import { isObjectSchema, isValidSchema } from './utils';

export const ParamsSchema = (schema: JSONSchema): MethodDecorator => {
  return (target, propertyKey, descriptor) => {
    const jsonSchema = schema.valueOf() as Record<
      string,
      SchemaObject | ReferenceObject
    >;
    if (isObjectSchema(jsonSchema)) {
      Object.entries(
        jsonSchema.properties as Record<string, SchemaObject | ReferenceObject>,
      ).forEach(([property, schema]) => {
        if (isValidSchema(schema)) {
          ApiParam({
            name: property,
            schema,
          })(target, propertyKey, descriptor);
        }
      });
    }
  };
};
