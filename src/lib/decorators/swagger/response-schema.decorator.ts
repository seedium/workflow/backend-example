import { ApiResponse, ApiResponseSchemaHost } from '@nestjs/swagger';
import { JSONSchema } from 'fluent-json-schema';
import {
  errorResponseSchema,
  validationErrorResponseSchema,
} from '@lib/schemas';

export interface ApiResponseSchema
  extends Omit<ApiResponseSchemaHost, 'schema'> {
  schema: JSONSchema;
}

const defaultMapErrorResponses: Record<number, ApiResponseSchema> = {
  400: {
    description:
      'User validation is failed and requires to change request body before make request again',
    schema: validationErrorResponseSchema,
  },
  401: {
    description: 'Token was not provided or token invalid',
    schema: errorResponseSchema,
  },
  403: {
    description: `Forbidden resource. User doesn't have required permissions to make an action on requested resource`,
    schema: errorResponseSchema,
  },
  404: {
    description: 'Url is invalid or resource was not found',
    schema: errorResponseSchema,
  },
  500: {
    description: 'Internal Server Error',
    schema: errorResponseSchema,
  },
  503: {
    description: 'Service is unavailable',
    schema: errorResponseSchema,
  },
};

export const ResponseSchema = (
  responseMap: Record<number, ApiResponseSchema> = {},
): MethodDecorator & ClassDecorator => {
  return (
    target: Object | Function,
    propertyKey?: string | symbol,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    descriptor?: TypedPropertyDescriptor<any>,
  ) => {
    Object.entries({
      ...defaultMapErrorResponses,
      ...responseMap,
    }).forEach(([statusCode, { schema, ...apiResponseOptions }]) => {
      if (propertyKey && descriptor) {
        ApiResponse({
          ...apiResponseOptions,
          status: parseInt(statusCode),
          schema: schema.valueOf(),
        })(target, propertyKey, descriptor);
      } else {
        ApiResponse({
          ...apiResponseOptions,
          status: parseInt(statusCode),
          schema: schema.valueOf(),
        })(target as Function);
      }
    });
  };
};
