import S from 'fluent-json-schema';

export const headersSchema = S.object()
  .required(['example-account'])
  .prop('example-account', S.string());
