import S from 'fluent-json-schema';
import { idSchema, nullable, timestampSchema } from './field.schema';

export const resourceObjectSchema = S.object()
  .required(['id', 'created_at', 'updated_at', 'deleted_at'])
  .prop('id', idSchema)
  .prop('created_at', timestampSchema)
  .prop('updated_at', timestampSchema)
  .prop('deleted_at', nullable(timestampSchema));
