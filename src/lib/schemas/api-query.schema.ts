import S, { ObjectSchema, JSONSchema } from 'fluent-json-schema';

export const dataOptionsSchema = S.object()
  .additionalProperties(false)
  .prop(
    'expand',
    S.array()
      .description(
        'Expand related data: `team` for retrieve, `data.team` for list',
      )
      .items(S.string()),
  )
  .prop(
    'include',
    S.array()
      .description('Include additional values in response')
      .items(S.string()),
  );

export const dateFilterSchema = S.object()
  .additionalProperties(false)
  .prop('gt', S.number())
  .prop('gte', S.number())
  .prop('lt', S.number())
  .prop('lte', S.number());

export const listOptionsSchema = S.object()
  .additionalProperties(false)
  .prop(
    'page',
    S.number().description('Page to get list with offset').minimum(0),
  )
  .prop(
    'limit',
    S.number()
      .description('Limit of get data element in the list')
      .minimum(0)
      .maximum(100),
  )
  .prop(
    'sort',
    S.array()
      .description('Sort by direction and field: +created_at/-created_at')
      .items(S.string()),
  )
  .prop('created', S.oneOf([S.number(), dateFilterSchema]))
  .extend(dataOptionsSchema) as ObjectSchema;

export const listResponseSchema = (schema: JSONSchema): ObjectSchema =>
  S.object()
    .prop('data', S.array().items(schema))
    .prop('total_count', S.number());
