import S, { JSONSchema, StringSchema } from 'fluent-json-schema';
import { constantsToArray } from '@utils';

export const timestampSchema = S.number().description(
  'Timestamp in unix format',
);

export const idSchema = S.string().description('ID with prefix of resource');

export const defaultLengthStringSchema = S.string().maxLength(255);

export const defaultMinStringSchema = S.string().minLength(1);

export const defaultMinMaxStringSchema = S.string().minLength(1).maxLength(255);

export const emailSchema = S.string().format('email');

export const urlSchema = S.string().maxLength(2048);

export const phoneNumberSchema = S.string().minLength(10).maxLength(13);

export const positiveIntegerSchema = S.integer().minimum(0);

export const fileNameSchema = S.string().pattern(
  /^[$&+:;=?@#|'<>.^*()%!\w,\s-]+\.[A-Za-z]{2,4}$/,
);

export const pathSchema = S.string().pattern(/^(\/[^\/]+){0,5}\/?$/gm);

/*
 * Mark schema as can be `null`.
 * @remarks
 * This helper useful when need to mark `nullable` only one schema
 * @example
 * ```ts
 * const objectSchema = S.object().prop('test', S.string());
 * const someOtherObjectSchema = S.object().prop('can_be_null', nullable(objectSchema));
 * ```
 * */
export const nullable = (...schemas: JSONSchema[]): JSONSchema => {
  return S.anyOf([S.null(), ...schemas]);
};

/*
 * Use if property can be id or expanded resource
 * */
export const foreignRef = (schema: JSONSchema): JSONSchema => {
  return S.anyOf([idSchema, schema]);
};

/*
 * Use if property can be id or expanded resource and `null`
 * */
export const nullableForeignRef = (schema: JSONSchema): JSONSchema =>
  nullable(idSchema, schema);

export const enumSchema = (enumObject: Record<string, string>): StringSchema =>
  S.string().enum(constantsToArray(enumObject));

export const trimStringSchema = (schema: StringSchema): JSONSchema =>
  schema.raw({ transform: ['trim'] });
