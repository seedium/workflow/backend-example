import { Knex } from 'knex';
import { tableName as accountTableName } from '../migrations/20220403090056_create_accounts_table';

export async function seed(knex: Knex, accounts = 2): Promise<any[]> {
  // Deletes ALL existing entries
  await knex.raw(`truncate ${accountTableName} cascade`);

  // Inserts seed entries
  return knex
    .batchInsert(
      accountTableName,
      new Array(accounts).fill(0).map((_value, index) => ({
        name: 'Test ' + (index + 1),
      })),
    )
    .returning('*');
}
