import { Knex } from 'knex';

export const timestamp = (knex: Knex): Knex.Raw =>
  knex.raw('round(extract(epoch from now()))');
