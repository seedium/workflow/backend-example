import { Knex } from 'knex';
import { createAccountResourceColumns } from '../utils';
import { userAccountRoleEnum } from './20220403090102_create_account_users_table';

export const tableName = 'invitations';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable(tableName, (table) => {
    createAccountResourceColumns(knex, table, 'inv');
    table.string('token').notNullable();
    table.string('email').notNullable();
    table
      .specificType('role', userAccountRoleEnum)
      .notNullable()
      .defaultTo('owner');
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable(tableName);
}
