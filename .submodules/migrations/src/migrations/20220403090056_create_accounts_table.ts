import { Knex } from 'knex';
import { createSoftResourceColumns } from '../utils';

export const tableName = 'accounts';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable(tableName, (table) => {
    createSoftResourceColumns(knex, table, 'acct');
    table.string('name').notNullable();
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable(tableName);
}
