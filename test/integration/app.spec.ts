import { createSandbox, SinonSandbox } from 'sinon';
import { expect } from '../core/asserts';
import { bootstrap } from '../../src/bootstrap';

describe('Application', () => {
  let sandbox: SinonSandbox;
  beforeEach(() => {
    sandbox = createSandbox();
  });
  describe('should bootstrap and start application', function () {
    this.timeout(100000);
    it('with all features enabled', async () => {
      const app = await bootstrap({ abortOnError: false });
      expect(app).property('isInitialized').is.true;
      expect(app).property('isListening').is.true;
      await app.close();
    });
    it('with all features disabled', async () => {
      const app = await bootstrap({ abortOnError: false });
      expect(app).property('isInitialized').is.true;
      expect(app).property('isListening').is.true;
      await app.close();
    });
  });
  afterEach(() => {
    sandbox.restore();
  });
});
