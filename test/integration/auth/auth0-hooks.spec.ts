import { NestFastifyApplication } from '@nestjs/platform-fastify';
import { default as faker } from 'faker';
import { AuthModule } from '@app/auth/auth.module';
import { buildApplication } from '../../core/lib/application';
import { clearAll } from '../../core/lib/database/repositories';
import { buildHttpRequest, HttpRequest } from '../../core/lib/http-request';
import { expect, expectErrorCode, expectStatusCode } from '../../core/asserts';
import { getRandomDatabaseSignUpRequestOptions } from '../../core/fixtures';
import { selectAllUsers, selectUserByEmail } from '../../core/selects';
import { insertRandomUser } from '../../core/seeds';

describe('Auth0 Hooks', () => {
  let app: NestFastifyApplication;
  let request: HttpRequest;

  before(async () => {
    app = await buildApplication(
      {
        imports: [AuthModule],
      },
      {
        auth0: {
          hooks: {
            apiKey: 'test_integration',
          },
        },
      },
    );
  });
  beforeEach(async () => {
    request = buildHttpRequest(app);
  });
  afterEach(async () => {
    await clearAll();
  });
  after(async () => {
    await app.close();
  });

  it('pre signup endpoint should be protected by key auth', async () => {
    const { payload, response } = await request('/auth/pre_signup', {
      method: 'POST',
      payload: { user: {}, request: {} },
    });
    expect(response.statusCode).eq(401);
    expect(payload).property('code').eq('api_key_required');
    const users = await selectAllUsers();
    expect(users).length(0);
  });
  it('post signup endpoint should be protected by key auth', async () => {
    const { payload, response } = await request('/auth/post_signup', {
      method: 'POST',
      payload: { user: {}, request: {} },
    });
    expect(response.statusCode).eq(401);
    expect(payload).property('code').eq('api_key_required');
    const users = await selectAllUsers();
    expect(users).length(0);
  });
  it('should throw return an error if api key is wrong', async () => {
    const { payload, response } = await request('/auth/pre_signup', {
      method: 'POST',
      payload: {
        user: {},
        request: {},
      },
      headers: {
        'X-API-Key': 'invalid_api_key',
        'content-type': 'application/json',
      },
    });
    expect(response.statusCode).eq(401);
    expect(payload).property('code').eq('api_key_required');
    const users = await selectAllUsers();
    expect(users).length(0);
  });

  describe('Pre Signup', () => {
    it('should create user in database', async () => {
      const preSignUpRequestOptions = getRandomDatabaseSignUpRequestOptions({
        app_metadata: {},
        email: faker.internet.email(),
        given_name: faker.name.firstName(),
        family_name: faker.name.lastName(),
        user_metadata: {},
      });
      const { response } = await request(
        '/auth/pre_signup',
        preSignUpRequestOptions,
      );
      expect(response.statusCode).eq(204);
      const createdUser = await selectUserByEmail(
        preSignUpRequestOptions.payload.user.email as string,
      );
      expect(createdUser)
        .an('object')
        .excluding(['id', 'created_at', 'updated_at', 'deleted_at'])
        .deep.eq({
          email: preSignUpRequestOptions.payload.user.email,
          first_name: preSignUpRequestOptions.payload.user.given_name,
          last_name: preSignUpRequestOptions.payload.user.family_name,
          sub: null,
          avatar_link: null,
          timezone: preSignUpRequestOptions.payload.request.geoip.timeZone,
          city: preSignUpRequestOptions.payload.request.geoip.cityName,
          country: preSignUpRequestOptions.payload.request.geoip.countryName,
        });
    });
    it('should throw an error if user with email already exists', async () => {
      const testEmail = faker.internet.email();
      await insertRandomUser({ email: testEmail });
      const preSignUpRequestOptions = getRandomDatabaseSignUpRequestOptions({
        app_metadata: {},
        email: testEmail,
        given_name: faker.name.firstName(),
        family_name: faker.name.lastName(),
        user_metadata: {},
      });
      const { response, payload } = await request(
        '/auth/pre_signup',
        preSignUpRequestOptions,
      );
      expect(response.statusCode).eq(400);
      expectErrorCode(payload, 'user_email_already_exists');
      const users = await selectAllUsers();
      expect(users).length(1);
    });
  });
  describe('Post Signup', () => {
    it('should update user in database with new user id and name', async () => {
      const postSignUpRequestOptions = getRandomDatabaseSignUpRequestOptions({
        email: faker.internet.email(),
        user_id: `auth0|${Date.now()}`,
        given_name: faker.name.firstName(),
        family_name: faker.name.lastName(),
        picture: faker.internet.url(),
        app_metadata: {},
        user_metadata: {},
        email_verified: false,
        phone_verified: false,
      });
      const user = await insertRandomUser({
        email: postSignUpRequestOptions.payload.user.email,
        first_name: postSignUpRequestOptions.payload.user.given_name,
        last_name: postSignUpRequestOptions.payload.user.family_name,
        avatar_link: postSignUpRequestOptions.payload.user.picture,
      });
      const { response } = await request(
        '/auth/post_signup',
        postSignUpRequestOptions,
      );
      expectStatusCode(response, 204);
      const createdUser = await selectUserByEmail(user.email);
      expect(createdUser)
        .an('object')
        .excluding(['id', 'created_at', 'updated_at', 'deleted_at'])
        .deep.eq({
          email: user.email,
          first_name: user.first_name,
          last_name: user.last_name,
          avatar_link: user.avatar_link,
          sub: (postSignUpRequestOptions.payload.user as any).user_id,
          timezone: postSignUpRequestOptions.payload.request.geoip.timeZone,
          city: postSignUpRequestOptions.payload.request.geoip.cityName,
          country: postSignUpRequestOptions.payload.request.geoip.countryName,
        });
    });
  });
});
