import { NestFastifyApplication } from '@nestjs/platform-fastify';
import * as faker from 'faker';
import { AccountModule } from '@app/account/account.module';
import { AccountUserRecord } from '@app/relations/account-user/interfaces';
import { buildApplication } from '../../core/lib/application';
import {
  insertRandomAccount,
  insertRandomUserWithAccount,
  insertRandomUserWithAuth0Sub,
} from '../../core/seeds';
import { buildHttpRequest, HttpRequest } from '../../core/lib/http-request';
import { expect, expectStatusCode } from '../../core/asserts';
import {
  accountUserRepository,
  clearAll,
} from '../../core/lib/database/repositories';
import {
  selectAccountById,
  selectAccountUserByAccount,
} from '../../core/selects';

describe('Account API', () => {
  let app: NestFastifyApplication;
  let request: HttpRequest;
  before(async () => {
    app = await buildApplication({
      imports: [AccountModule],
    });
    request = buildHttpRequest(app);
  });
  afterEach(async () => {
    await clearAll();
  });
  after(async () => {
    await app.close();
  });
  describe('POST /accounts', () => {
    it('should create an account', async () => {
      const { token } = await insertRandomUserWithAuth0Sub();
      const fixtureAccountPayload = {
        name: faker.random.word(),
      };
      const { response, payload } = await request('/accounts', {
        method: 'POST',
        payload: fixtureAccountPayload,
        jwt: token,
      });
      expectStatusCode(response, 201);
      expect(payload)
        .excluding(['id', 'created_at', 'updated_at', 'deleted_at'])
        .deep.eq(fixtureAccountPayload);
    });
    it('should create account user record', async () => {
      const { user, token } = await insertRandomUserWithAuth0Sub();
      const { response, payload } = await request<AccountUserRecord>(
        '/accounts',
        {
          method: 'POST',
          payload: {
            name: faker.random.word(),
          },
          jwt: token,
        },
      );
      expectStatusCode(response, 201);
      const accountUsers = await accountUserRepository().select<
        AccountUserRecord[]
      >('*');
      expect(accountUsers).length(1);
      const [accountUser] = accountUsers;
      expect(accountUser)
        .excluding(['id', 'created_at', 'updated_at', 'deleted_at'])
        .deep.eq({
          account: payload.id,
          user: user.id,
          role: 'owner',
        });
    });
  });
  describe('GET /accounts/{id_account}', () => {
    it('should retrieve account', async () => {
      const { token, account } = await insertRandomUserWithAccount();
      const { response, payload } = await request<AccountUserRecord>(
        `/accounts/${account.id}`,
        {
          jwt: token,
        },
      );
      expectStatusCode(response, 200);
      expect(payload).deep.eq(account);
    });
    it('should return an error if trying to get foreign account', async () => {
      const [{ token }, { account }] = await Promise.all([
        insertRandomUserWithAccount(),
        insertRandomUserWithAccount(),
      ]);
      const { response } = await request<AccountUserRecord>(
        `/accounts/${account.id}`,
        {
          jwt: token,
        },
      );
      expectStatusCode(response, 404);
    });
  });
  describe('PATCH /accounts/{id_account}', () => {
    it('should update account', async () => {
      const { token, account } = await insertRandomUserWithAccount();
      const fixtureUpdateBody = {
        name: faker.random.word(),
      };
      const { response, payload } = await request<AccountUserRecord>(
        `/accounts/${account.id}`,
        {
          method: 'PATCH',
          payload: fixtureUpdateBody,
          jwt: token,
        },
      );
      expectStatusCode(response, 200);
      expect(payload)
        .excluding(['updated_at'])
        .deep.eq({
          ...account,
          ...fixtureUpdateBody,
        });
    });
    it('should return an error if trying to update foreign account', async () => {
      const [{ token }, { account }] = await Promise.all([
        insertRandomUserWithAccount(),
        insertRandomUserWithAccount(),
      ]);
      const fixtureUpdateBody = {
        name: faker.random.word(),
      };
      const { response } = await request<AccountUserRecord>(
        `/accounts/${account.id}`,
        {
          method: 'PATCH',
          payload: fixtureUpdateBody,
          jwt: token,
        },
      );
      expectStatusCode(response, 404);
      const notUpdatedAccount = await selectAccountById(account.id);
      expect(notUpdatedAccount).deep.eq(account);
    });
  });
  describe('DELETE /accounts/{id_account}', () => {
    it('should delete account', async () => {
      const { token, account } = await insertRandomUserWithAccount();
      const { response } = await request<AccountUserRecord>(
        `/accounts/${account.id}`,
        {
          method: 'DELETE',
          jwt: token,
        },
      );
      expectStatusCode(response, 200);
      const deletedAccount = await selectAccountById(account.id);
      expect(deletedAccount).property('deleted_at').is.not.null;
    });
    it('should delete account user alongside account', async () => {
      const { token, account } = await insertRandomUserWithAccount();
      const { response } = await request<AccountUserRecord>(
        `/accounts/${account.id}`,
        {
          method: 'DELETE',
          jwt: token,
        },
      );
      expectStatusCode(response, 200);
      const accountUsers = await selectAccountUserByAccount(account.id);
      accountUsers.forEach(
        (accountUser) => expect(accountUser).property('deleted_at').is.not.null,
      );
    });
    it('should return an error if trying to delete foreign account', async () => {
      const [{ token }, account] = await Promise.all([
        insertRandomUserWithAccount(),
        insertRandomAccount(),
      ]);
      const { response } = await request<AccountUserRecord>(
        `/accounts/${account.id}`,
        {
          method: 'DELETE',
          jwt: token,
        },
      );
      expectStatusCode(response, 404);
      const deletedAccount = await selectAccountById(account.id);
      expect(deletedAccount).property('deleted_at').is.null;
    });
  });
});
