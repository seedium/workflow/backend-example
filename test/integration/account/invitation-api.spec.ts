import { NestFastifyApplication } from '@nestjs/platform-fastify';
import * as faker from 'faker';
import { LightMyRequestResponse } from 'fastify';
import { AccountModule } from '@app/account/account.module';
import { InvitationRecord, AccountRecord } from '@app/account/interfaces';
import { UserRecord } from '@app/user/interfaces';
import { buildApplication } from '../../core/lib/application';
import { clearAll } from '../../core/lib/database/repositories';
import { buildHttpRequest, HttpRequest } from '../../core/lib/http-request';
import {
  insertRandomInvitation,
  insertRandomAccountUser,
  insertRandomUser,
  insertRandomUserWithAccount,
  insertRandomUserWithAuth0Sub,
} from '../../core/seeds';
import { expect, expectErrorCode, expectStatusCode } from '../../core/asserts';
import {
  selectInvitationById,
  selectAccountUserByUser,
} from '../../core/selects';

describe('Invitation API', () => {
  let app: NestFastifyApplication;
  let request: HttpRequest;
  let testAccount: AccountRecord;
  before(async () => {
    app = await buildApplication({
      imports: [AccountModule],
    });
  });
  beforeEach(async () => {
    const { token, account } = await insertRandomUserWithAccount();
    request = buildHttpRequest(app, {
      jwt: token,
      idAccount: account.id,
    });
    testAccount = account;
  });
  afterEach(async () => {
    await clearAll();
  });
  after(async () => {
    await app.close();
  });
  describe('GET /invitations', () => {
    it('should get list of invitations', async () => {
      const testInvitation = await insertRandomInvitation({
        account: testAccount.id,
      });
      const { response, payload } = await request('/invitations');
      expectStatusCode(response, 200);
      expect(payload)
        .excludingEvery(['account'])
        .deep.eq({
          data: [testInvitation],
        });
    });
  });
  describe('POST /invitations', () => {
    it('should create new invitation', async () => {
      const fixtureCreateInvitation = {
        email: faker.internet.email(),
        role: 'owner',
      };
      const { response, payload } = await request('/invitations', {
        method: 'POST',
        payload: fixtureCreateInvitation,
      });
      expectStatusCode(response, 201);
      expect(payload)
        .excluding(['id', 'created_at', 'updated_at', 'deleted_at', 'token'])
        .deep.eq(fixtureCreateInvitation);
      expect(payload).property('token').an('string');
    });
    it('should not create new invitation if user already in account', async () => {
      const invitedUser = await insertRandomUser();
      await insertRandomAccountUser({
        account: testAccount.id,
        user: invitedUser.id,
      });
      const fixtureCreateInvitation = {
        email: invitedUser.email,
        role: 'owner',
      };
      const { response, payload } = await request('/invitations', {
        method: 'POST',
        payload: fixtureCreateInvitation,
      });
      expectStatusCode(response, 400);
      expectErrorCode(payload, 'user_already_in_account');
    });
    it('should not create account invitation twice', async () => {
      const exitingInvitation = await insertRandomInvitation({
        account: testAccount.id,
      });
      const { response, payload } = await request('/invitations', {
        method: 'POST',
        payload: {
          email: exitingInvitation.email,
          role: exitingInvitation.role,
        },
      });
      expectStatusCode(response, 400);
      expectErrorCode(payload, 'user_already_invited');
    });
  });
  describe('GET /invitations/{id_account_invitation}', () => {
    it('should retrieve invitation', async () => {
      const testInvitation = await insertRandomInvitation({
        account: testAccount.id,
      });
      const { response, payload } = await request(
        `/invitations/${testInvitation.id}`,
      );
      expectStatusCode(response, 200);
      expect(payload).excluding(['account']).deep.eq(testInvitation);
    });
  });
  describe('DELETE /invitations/{id_account_invitation}', () => {
    it('should delete invitation', async () => {
      const testtInvitation = await insertRandomInvitation({
        account: testAccount.id,
      });
      const { response, payload } = await request(
        `/invitations/${testtInvitation.id}`,
        {
          method: 'DELETE',
        },
      );
      expectStatusCode(response, 200);
      expect(payload).property('deleted_at').is.not.null;
    });
  });
  describe('GET /invitations/{token}/retrieve', () => {
    it('should retrieve account invitation with account name without authentication', async () => {
      const testInvitation = await insertRandomInvitation({
        account: testAccount.id,
      });
      const { response, payload } = await request(
        `/invitations/${testInvitation.token}/retrieve`,
        {
          idAccount: null,
          jwt: null,
        },
      );
      expectStatusCode(response, 200);
      expect(payload)
        .excluding(['account'])
        .deep.eq({
          ...testInvitation,
          account_name: testAccount.name,
        });
    });
  });
  describe('POST /invitations/{token}/accept', () => {
    describe('should successfully accept', () => {
      let response: LightMyRequestResponse;
      let testAcceptedUser: UserRecord;
      let testAcceptedInvitation: InvitationRecord;
      beforeEach(async () => {
        const testInvitation = await insertRandomInvitation({
          account: testAccount.id,
        });
        const { user, token } = await insertRandomUserWithAuth0Sub({
          email: testInvitation.email,
        });
        const result = await request(
          `/invitations/${testInvitation.token}/accept`,
          {
            method: 'POST',
            idAccount: null,
            jwt: token,
          },
        );
        response = result.response;
        testAcceptedUser = user;
        testAcceptedInvitation = testInvitation;
      });
      it('should be status code 204', () => {
        expectStatusCode(response, 204);
      });
      it('should create new account user record', async () => {
        const accountUsers = await selectAccountUserByUser(testAcceptedUser.id);
        expect(accountUsers).length(1);
        const [accountUser] = accountUsers;
        expect(accountUser)
          .excluding(['id', 'created_at', 'updated_at', 'deleted_at'])
          .deep.eq({
            user: testAcceptedUser.id,
            account: testAcceptedInvitation.account,
            role: testAcceptedInvitation.role,
          });
      });
      it('should remove account invitation', async () => {
        const deletedAcceptedInvitation = await selectInvitationById(
          testAcceptedInvitation.id,
        );
        expect(deletedAcceptedInvitation).property('deleted_at').is.not.null;
      });
    });
    describe('should not accept invitation with another email', () => {
      let response: LightMyRequestResponse;
      let payload: unknown;
      let testUnsuccessfullyAcceptedUser: UserRecord;
      let testUnsuccessfullyAcceptedInvitation: InvitationRecord;
      beforeEach(async () => {
        const testInvitation = await insertRandomInvitation({
          account: testAccount.id,
        });
        const { user, token } = await insertRandomUserWithAuth0Sub();
        const result = await request(
          `/invitations/${testInvitation.token}/accept`,
          {
            method: 'POST',
            idAccount: null,
            jwt: token,
          },
        );
        response = result.response;
        payload = result.payload;
        testUnsuccessfullyAcceptedUser = user;
        testUnsuccessfullyAcceptedInvitation = testInvitation;
      });
      it('should be status code 400 and error code `wrong_invitation_email`', () => {
        expectStatusCode(response, 400);
        expectErrorCode(payload, 'wrong_invitation_email');
      });
      it('should not create new account', async () => {
        const accountUsers = await selectAccountUserByUser(
          testUnsuccessfullyAcceptedUser.id,
        );
        expect(accountUsers).length(0);
      });
      it('should not remove account invitation', async () => {
        const deletedAcceptedInvitation = await selectInvitationById(
          testUnsuccessfullyAcceptedInvitation.id,
        );
        expect(deletedAcceptedInvitation).property('deleted_at').is.null;
      });
    });
  });
  describe('POST /invitations/{token}/reject', () => {
    describe('should successfully reject', () => {
      let response: LightMyRequestResponse;
      let testRejectedUser: UserRecord;
      let testRejectedInvitation: InvitationRecord;
      beforeEach(async () => {
        const testInvitation = await insertRandomInvitation({
          account: testAccount.id,
        });
        const { user, token } = await insertRandomUserWithAuth0Sub({
          email: testInvitation.email,
        });
        const result = await request(
          `/invitations/${testInvitation.token}/reject`,
          {
            method: 'POST',
            idAccount: null,
            jwt: token,
          },
        );
        response = result.response;
        testRejectedUser = user;
        testRejectedInvitation = testInvitation;
      });
      it('should be status code 204', () => {
        expectStatusCode(response, 204);
      });
      it('should not create new account user record', async () => {
        const accountUsers = await selectAccountUserByUser(testRejectedUser.id);
        expect(accountUsers).length(0);
      });
      it('should remove account invitation', async () => {
        const deletedAcceptedInvitation = await selectInvitationById(
          testRejectedInvitation.id,
        );
        expect(deletedAcceptedInvitation).property('deleted_at').is.not.null;
      });
    });
    describe('should not reject invitation with another email', () => {
      let response: LightMyRequestResponse;
      let payload: unknown;
      let testUnsuccessfullyRejectedInvitation: InvitationRecord;
      beforeEach(async () => {
        const testInvitation = await insertRandomInvitation({
          account: testAccount.id,
        });
        const { token } = await insertRandomUserWithAuth0Sub();
        const result = await request(
          `/invitations/${testInvitation.token}/reject`,
          {
            method: 'POST',
            idAccount: null,
            jwt: token,
          },
        );
        response = result.response;
        payload = result.payload;
        testUnsuccessfullyRejectedInvitation = testInvitation;
      });
      it('should be status code 400 and error code `wrong_invitation_email`', () => {
        expectStatusCode(response, 400);
        expectErrorCode(payload, 'wrong_invitation_email');
      });
      it('should not remove account invitation record', async () => {
        const deletedAcceptedInvitation = await selectInvitationById(
          testUnsuccessfullyRejectedInvitation.id,
        );
        expect(deletedAcceptedInvitation).property('deleted_at').is.null;
      });
    });
  });
});
