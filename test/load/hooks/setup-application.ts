import { NestFastifyApplication } from '@nestjs/platform-fastify';
import { bootstrap } from '../../../src/bootstrap';

export let app: NestFastifyApplication;
export let appUrl: string;

before(async () => {
  app = await bootstrap({
    logger: false,
  });
  appUrl = await app.getUrl();
});

after(async () => {
  await app.close();
});
