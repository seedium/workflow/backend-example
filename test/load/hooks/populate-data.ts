import { AccountUserRecord } from '@app/relations/account-user/interfaces';
import { clearAll } from '../../core/lib/database/repositories';
import { insertRandomAccountUser } from '../../core/seeds';

export let randomAccountUser: AccountUserRecord;

before(async () => {
  randomAccountUser = await insertRandomAccountUser();
});

after(async () => {
  await clearAll();
});
