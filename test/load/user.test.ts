import autocannon from 'autocannon';
import * as chai from 'chai';
import { appUrl } from './hooks/setup-application';
import { randomAccountUser } from './hooks/populate-data';
import { testCaseResults } from './hooks/render-result';

const expect = chai.expect;

describe('User Load', () => {
  describe('GET /profile/current_user', function () {
    let result;
    before(async () => {
      result = await autocannon({
        url: `${appUrl}/v1/profile/current_user`,
        connections: 10,
        pipelining: 1,
        duration: 60,
        warmup: {
          connections: 1,
          duration: 2,
        },
        headers: {
          'example-dev-user-id': randomAccountUser.user,
          'example-account': randomAccountUser.account,
        },
      });
      testCaseResults.push({
        testCase: this.fullTitle(),
        result,
      });
    });
    it('average latency should be less 20ms', () => {
      expect(result.latency.average).lessThan(20);
    });
    it('average rps should be greater than 800', () => {
      expect(result.requests.average).greaterThan(800);
    });
  });
});
