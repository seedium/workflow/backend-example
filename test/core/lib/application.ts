import { randomUUID } from 'crypto';
import { parse as qsParse } from 'qs';
import { createStubInstance } from 'sinon';
import { ModuleMetadata, Logger } from '@nestjs/common';
import { FastifyInstance } from 'fastify';
import { Scope } from '@sentry/node';
import { Test, TestingModuleBuilder } from '@nestjs/testing';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { ValidationModule } from 'nestjs-validation';
import { CoreModule } from '@modules/core/core.module';
import { CqrsModule } from '@modules/cqrs/cqrs.module';
import { MetadataContainer } from '@modules/core/metadata';
import { ApplicationFastifyRequest } from '@modules/core/interfaces';
import { ConfigModule } from '@modules/config/config.module';
import { Auth0Service, JwtService } from '@app/auth/services';
import { mockConfig, MockLoggerService, MockJwtService } from '../mocks';

export const buildApplication = async (
  metadata: ModuleMetadata,
  config?: Record<string, unknown>,
  beforeCompile?: (
    app: TestingModuleBuilder,
  ) => TestingModuleBuilder | Promise<TestingModuleBuilder>,
): Promise<NestFastifyApplication> => {
  let appModule = Test.createTestingModule({
    ...metadata,
    imports: [
      ConfigModule,
      ValidationModule.forRoot(),
      CoreModule,
      CqrsModule.forRoot(),
      ...(metadata.imports || []),
    ],
  });

  await mockConfig(appModule, config);
  appModule.overrideProvider(Logger).useClass(MockLoggerService);
  appModule.overrideProvider(JwtService).useClass(MockJwtService);
  appModule
    .overrideProvider(Auth0Service)
    .useValue(createStubInstance(Auth0Service));

  if (beforeCompile) {
    appModule = await beforeCompile(appModule);
  }

  const compiledApp = await appModule.compile();
  const app = compiledApp.createNestApplication<NestFastifyApplication>(
    new FastifyAdapter({
      disableRequestLogging: true,
      genReqId: () => randomUUID(),
      querystringParser: (s) => qsParse(s),
    }),
  );

  const fastify = app.getHttpAdapter().getInstance() as FastifyInstance;
  fastify.addHook('onRequest', (req, _, done) => {
    (req as ApplicationFastifyRequest).metadata = new MetadataContainer(
      randomUUID(),
    );
    (req as ApplicationFastifyRequest).sentryScope = new Scope();
    done();
  });
  await app.init();
  return app;
};
