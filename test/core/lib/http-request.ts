import { stringify as qsStringify } from 'qs';
import {
  Response as LightMyRequestResponse,
  InjectOptions,
} from 'light-my-request';
import { NestFastifyApplication } from '@nestjs/platform-fastify';

export interface HttpRequestOptions extends Omit<InjectOptions, 'query'> {
  jwt?: string | null;
  idAccount?: string | null;
  query?: Record<string, unknown>;
}

export type HttpRequestResult<T> = Promise<{
  response: LightMyRequestResponse;
  payload: T;
}>;

export type HttpRequest = <T>(
  path: string,
  options?: HttpRequestOptions,
) => HttpRequestResult<T>;

const valueOrUndefined = <T>(
  val: T,
  parentVal: T | undefined,
): T | undefined => {
  if (val === null) {
    return undefined;
  }
  return val ?? parentVal;
};

export const buildHttpRequest = (
  app: NestFastifyApplication,
  parentOptions?: HttpRequestOptions,
): HttpRequest => {
  return async <T>(
    path: string,
    options: HttpRequestOptions = {},
  ): HttpRequestResult<T> => {
    let { jwt, idAccount, payload, headers = {} } = options;
    const { query, method = 'GET', ...injectOptions } = options;
    jwt = valueOrUndefined(jwt, parentOptions?.jwt);
    idAccount = valueOrUndefined(idAccount, parentOptions?.idAccount);
    if (typeof payload === 'object') {
      headers = {
        ...headers,
        'content-type': 'application/json',
      };
      payload = JSON.stringify(payload);
    }
    if (typeof query === 'object') {
      path = `${path}?${qsStringify(query)}`;
    }
    if (idAccount) {
      headers = {
        ...headers,
        'example-account': idAccount,
      };
    }
    if (jwt) {
      headers = {
        ...headers,
        authorization: `Bearer ${jwt}`,
      };
    }
    headers = {
      ...headers,
      ...(parentOptions?.headers || {}),
    };
    const response = await app.inject({
      path,
      method,
      headers,
      payload,
      ...injectOptions,
    });
    return {
      response,
      payload: (response.payload ? response.json() : response.payload) as T,
    };
  };
};
