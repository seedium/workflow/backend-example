import { Knex } from 'knex';
import { UserRecord } from '@app/user/interfaces';
import { InvitationRecord, AccountRecord } from '@app/account/interfaces';
import { AccountUserRecord } from '@app/relations/account-user/interfaces';
import { getKnexManager } from './knex';

export type EntityRepository<T = unknown> = () => Knex.QueryBuilder<T>;

export const accountRepository: EntityRepository<AccountRecord> = () =>
  getKnexManager()('accounts');

export const accountUserRepository: EntityRepository<AccountUserRecord> = () =>
  getKnexManager()('account_users');

export const invitationRepository: EntityRepository<InvitationRecord> = () =>
  getKnexManager()('invitations');

export const userRepository: EntityRepository<UserRecord> = () =>
  getKnexManager()('users');

export const clearAll = async (
  repositories?: EntityRepository[],
): Promise<void> => {
  if (!repositories) {
    repositories = [
      accountRepository,
      accountUserRepository,
      invitationRepository,
      userRepository,
    ];
  }

  const tables_string = repositories
    .map((repository) => {
      const repo = repository() as unknown as { _single: { table: string } };
      return repo._single.table;
    })
    .join(',');

  await getKnexManager().raw(`truncate ${tables_string} cascade`);
};
