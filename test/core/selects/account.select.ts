import { AccountRecord } from '@app/account/interfaces';
import { accountRepository } from '../lib/database/repositories';

export function selectAccountById(id: string): Promise<AccountRecord> {
  return accountRepository().where('id', id).first<AccountRecord>();
}
