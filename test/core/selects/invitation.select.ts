import { InvitationRecord } from '@app/account/interfaces';
import { invitationRepository } from '../lib/database/repositories';

export function selectInvitationById(id: string): Promise<InvitationRecord> {
  return invitationRepository().where('id', id).first<InvitationRecord>();
}
