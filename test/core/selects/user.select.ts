import { UserRecord } from '@app/user/interfaces';
import { userRepository } from '../lib/database/repositories';

export function selectAllUsers(): Promise<UserRecord> {
  return userRepository().select('*');
}

export function selectUserByEmail(email: string): Promise<UserRecord> {
  return userRepository().select('*').where('email', email).first<UserRecord>();
}

export function selectUserById(id: string): Promise<UserRecord> {
  return userRepository().select('*').where('id', id).first<UserRecord>();
}
