import { datatype } from 'faker';
import { ID, IResourceObject } from '@lib/db';

export const generateRandomArray = <T>(
  mapReducer: () => T,
  length: number = datatype.number({
    min: 1,
    max: 10,
  }),
): T[] => new Array(length).fill(0).map(mapReducer);

export const getIdOrCreateNewResource = async (
  id: ID | undefined,
  insert: () => Promise<IResourceObject>,
): Promise<ID> => {
  return id ?? (await insert()).id;
};
