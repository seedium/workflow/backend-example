import { expect } from 'chai';
import { Response as LightMyRequestResponse } from 'light-my-request';

export const expectStatusCode = (
  response: LightMyRequestResponse,
  statusCode: number,
): Chai.Assertion =>
  expect(response.statusCode).eq(
    statusCode,
    response.payload ? response.json().message : undefined,
  );

export const expectErrorCode = <T = { code: string }>(
  payload: T,
  code: string,
): Chai.Assertion => expect(payload).property('code').eq(code);
