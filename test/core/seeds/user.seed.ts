import { UserRecord, User } from '@app/user/interfaces';
import { AccountRecord } from '@app/account/interfaces';
import { AccountUserRecord } from '@app/relations/account-user/interfaces';
import { userRepository } from '../lib/database/repositories';
import { getMockJwt } from '../mocks';
import { getRandomUser } from '../fixtures';
import { insertRandomAccount } from './account.seed';
import { insertRandomAccountUser } from './account-user.seed';

export const insertRandomUser = async (
  userOverride: Partial<UserRecord> = {},
): Promise<User> => {
  const [record] = await userRepository()
    .insert(
      getRandomUser({
        id: undefined,
        ...userOverride,
      }),
    )
    .returning<UserRecord[]>('*');
  return record;
};

export const insertRandomUserWithAuth0Sub = async (
  userOverride: Partial<UserRecord> = {},
): Promise<{
  token: string;
  user: UserRecord;
}> => {
  const randomId = `auth0|${Date.now()}`;
  const [token, user] = await Promise.all([
    getMockJwt(randomId),
    insertRandomUser({ ...userOverride, sub: randomId }),
  ]);
  return {
    token,
    user,
  };
};

export const insertRandomUserWithAccount = async (
  override: {
    user?: Partial<UserRecord>;
    account?: Partial<AccountRecord>;
    accountUser?: Partial<AccountUserRecord>;
  } = {
    user: {},
    account: {},
    accountUser: {},
  },
): Promise<{
  token: string;
  user: UserRecord;
  account: AccountRecord;
  accountUser: AccountUserRecord;
}> => {
  const { token, user } = await insertRandomUserWithAuth0Sub({
    ...override.user,
  });
  const account = await insertRandomAccount(override.account);
  const accountUser = await insertRandomAccountUser({
    user: user.id,
    account: account.id,
    ...override.accountUser,
  });
  return {
    token,
    user,
    account,
    accountUser,
  };
};
