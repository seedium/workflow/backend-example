import * as faker from 'faker';
import { InvitationRecord } from '@app/account/interfaces';
import { now } from '@utils';

export const getRandomInvitation = (
  invitationOverride: Partial<InvitationRecord> = {},
): InvitationRecord => ({
  id: faker.datatype.uuid(),
  created_at: now(),
  updated_at: now(),
  deleted_at: null,
  account: faker.datatype.uuid(),
  email: faker.internet.email(),
  token: faker.datatype.uuid(),
  role: 'owner',
  ...invitationOverride,
});
