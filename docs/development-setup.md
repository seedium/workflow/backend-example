## Setup

- [Initial setup](#initial-setup)
- [Start application](#start-application)
  - [Start application in docker](#start-application-docker)
- [Cloud storage](#cloud-storage)
- [Testing](#testing)
- [Scripts](#scripts)

### <a name="initial-setup"></a> Initial setup

For deploy project locally, please follow the next guide:

1. **Clone project**

   ```bash
   $ git clone git@gitlab.com:seedium/workflow/backend-example.git
   ```

1. **Enviroment setup**

   The project is using various technology and adheres the approach as simple as possible.

   > Note! Do not override the default values. Use environment variables instead!

   Please create `.env` file with all variables that are in `.env.exmaple` file.

1. **Install dependencies**

   ```bash
   $ yarn install
   ```

1. <a name="required-tools"></a> **Required tools**

   > Note! Running database and other external dependencies required for application working

   - Postgres - Database. [[Docs](https://www.postgresql.org/docs/), [Image](https://hub.docker.com/_/postgres)]

   \
   All tools config and startup workflow described in `docker-compose.yml` file

   ```bash
   $ docker-compose -p example up -d
   ```

   or

   ```bash
   $ docker compose -p example up -d
   ```

   > Note! If one of the tools in docker-compose terminates with an error "EXITED(137)" you need up memory limits. Go to this [docs](https://docs.docker.com/config/containers/resource_constraints/) or update settings in DockerDesktop.

### <a name="start-application"></a> Start application

If the required tools are not running, return to step [Required tools](#required-tools)

```shell
$ yarn run build
$ yarn run start
```

or run application in watch mode

```shell
$ yarn run start:dev
```

#### <a name="start-application-docker"></a> Start application in docker

If you are not going to develop or make changes in backend repository,
you can run backend in docker container with all dependencies with one command

```shell
$ yarn docker:start
```

> **IMPORTANT**. Before run command `docker:start` you need create empty `.env` file in the root of repository.
>
> ```shell
> $ touch .env
> ```
>
> If you accidentally executed `yarn docker:start` before create `.env` file and faced with the issue while running, you need to apply following commands
>
> ```shell
> $ rm -rf .env/ # Remove mounted `.env` directory
> $ docker container rm example # Remove container that was stuck
> $ touch .env # Create empty file
> $ yarn docker:start # Try to start new docker container
> ```

This command is more appropriate for team who is going just to consume backend API,
for instance, frontend team who is developing web or mobile client

If you would like to rebuild docker container, run `yarn docker:start` again.

> Note! If you pulled new changes or switched to another branch, docker container won't rebuild automatically! You need to run `yarn docker:start` every time when something changed in repository code.
> This command is idempotent, this mean you can run command as many times as you want. So to be sure that you are using the right backend version, don't hesitate to run command in any case.

To stop backend and all dependencies, run the next command

```shell
$ yarn docker:stop
```

To check current backend logs

```shell
$ yarn docker:logs
```

For more scenarios check the [scripts](#scripts) section.

### <a name="cloud-storage"></a> Cloud storage

### Images

**NOT IMPLEMENTED**
TODO Need to gcp storage emulator

### <a name="testing"></a> Testing

The unit tests

```shell
$ yarn test:unit
```

The integration tests

Integration tests work only with the [Required tools](#required-tools)

```shell
$ yarn test:integration
```

All tests

```shell
$ yarn test
```

### <a name="scripts"></a> Scripts

Make production build and save output to `dist` folder

```bash
$ yarn build
```

Start project in development mode

```bash
$ yarn start
```

Start project in development with auto reloading (Requires node.js => 14)

```bash
$ yarn start:dev
```

Start project and all dependencies in docker compose

```bash
$ yarn docker:start
```

Restart all services

```shell
$ yarn docker:restart # This command will restart all containers including database, redis, etc
$ yarn docker:restart example # This command will restart only backend container
```

Stop project and all dependencies in docker compose

```bash
$ yarn docker:stop
```

Open project logs for backend which is running in the docker compose

```shell
$ yarn docker:logs
```

Start project in development with auto reloading and debugger attached

```bash
$ yarn start:debug
```

Start project in production mode

```bash
$ yarn start:prod
```

Remove folder `dist`

```bash
$ yarn clean
```

Lint project files with Eslint

```bash
$ yarn lint
```

Lint all tests files in the project with Eslint

```bash
$ yarn lint:test
```

Format all project files with Prettier

```bash
$ yarn format
```

Run unit tests

```bash
$ yarn test:unit
```

Run integration tests (Requires docker-compose up -d before)

```bash
$ yarn test:integration
```
