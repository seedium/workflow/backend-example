import { ArgumentsCamelCase, Argv } from 'yargs';
import { OpenapiCommandOptions } from './interfaces';
import { readPackageJson } from '../../../../../src/utils';
import { setupApplication } from '../../../../../src/bootstrap/setup-application';
import { setupSwagger } from '../../../../../src/bootstrap/setup-swagger';
import { resolve, dirname } from 'path';
import { writeFile, mkdir } from 'fs/promises';

export const command = 'openapi';
export const desc = 'Generate openapi json';

export const builder = (yargs: Argv) => {
  yargs.option('output', {
    alias: 'o',
    default: 'openapi.json',
    describe: 'Where to put generated openapi json',
    type: 'string',
  });
};

export const handler = async (
  argv: ArgumentsCamelCase<OpenapiCommandOptions>,
): Promise<void> => {
  console.log('Start generating openapi');
  const packageJson = readPackageJson();
  const app = await setupApplication();
  const swagger = await setupSwagger(app, {
    version: packageJson.version,
    environment: 'local',
  });
  const savePath = resolve(process.cwd(), argv.output);
  const directory = dirname(savePath);
  await mkdir(directory, { recursive: true });
  await writeFile(savePath, JSON.stringify(swagger));
  await app.close();
  console.log(`Json file saved to ${savePath}`);
};
