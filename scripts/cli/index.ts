#!/usr/bin/env node
import yargs from 'yargs';
import { getCommandDirOptions } from './helpers';

yargs(process.argv.slice(2))
  .scriptName('yarn example')
  .usage('$0 <cmd> [args]')
  .commandDir('commands/create', getCommandDirOptions())
  .demandCommand()
  .showHelpOnFail(false)
  .help().argv;
